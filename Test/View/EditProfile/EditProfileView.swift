//
//  EditProfileView.swift
//  Test
//
//  Created by Hera on 07/12/20.
//

import SwiftUI

struct EditProfileView: View {
    
    @State var username: String = ""
    @State var password: String = ""
    
    @State private var isEditing = true
    
    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            VStack(alignment: .leading, spacing: 10) {
                Text("Nama")
                TextField("", text: $username)
                    .padding()
                    .background(Color.white)
                    .cornerRadius(5)
                    .shadow(radius: 3)
                    .onTapGesture(perform: {
                        isEditing = true
                    })
                    .animation(.default)
                Text("Email")
                TextField("", text: $username)
                    .padding()
                    .background(Color.white)
                    .cornerRadius(5)
                    .shadow(radius: 3)
                    .onTapGesture(perform: {
                        isEditing = true
                    })
                    .animation(.default)
                Group {
                    Text("Provinsi")
                    HStack {
                        TextField("", text: $username)
                            .padding()
                            .background(Color.white)
                            .cornerRadius(5)
                            .shadow(radius: 3)
                            .onTapGesture(perform: {
                                isEditing = true
                            })
                            .animation(.default)
                        Image(systemName: "chevron.down")
                            .padding()
                    }
                    .background(Color.white)
                    .cornerRadius(10)
                    .shadow(radius: 5)
                    Text("Kota")
                    HStack {
                        TextField("", text: $username)
                            .padding()
                            .background(Color.white)
                            .cornerRadius(5)
                            .shadow(radius: 3)
                            .onTapGesture(perform: {
                                isEditing = true
                            })
                            .animation(.default)
                        Image(systemName: "chevron.down")
                            .padding()
                    }
                    .background(Color.white)
                    .cornerRadius(10)
                    .shadow(radius: 5)
                    Text("Kecamatan")
                    HStack {
                        TextField("", text: $username)
                            .padding()
                            .background(Color.white)
                            .cornerRadius(5)
                            .shadow(radius: 3)
                            .onTapGesture(perform: {
                                isEditing = true
                            })
                            .animation(.default)
                        Image(systemName: "chevron.down")
                            .padding()
                    }
                    .background(Color.white)
                    .cornerRadius(10)
                    .shadow(radius: 5)
                    Text("Kelurahan / Desa")
                    HStack {
                        TextField("", text: $username)
                            .padding()
                            .background(Color.white)
                            .cornerRadius(5)
                            .shadow(radius: 3)
                            .onTapGesture(perform: {
                                isEditing = true
                            })
                            .animation(.default)
                        Image(systemName: "chevron.down")
                            .padding()
                    }
                    .background(Color.white)
                    .cornerRadius(10)
                    .shadow(radius: 5)
                }
                Group {
                    Text("Alamat Lengkap")
                    HStack {
                        TextField("", text: $username)
                            .padding()
                            .background(Color.white)
                            .cornerRadius(5)
                            .shadow(radius: 3)
                            .onTapGesture(perform: {
                                isEditing = true
                            })
                            .animation(.default)
                    }
                    .background(Color.white)
                    .cornerRadius(10)
                    .shadow(radius: 5)
                    Text("Tanggal Lahir")
                    HStack {
                        TextField("", text: $username)
                            .padding()
                            .background(Color.white)
                            .cornerRadius(5)
                            .shadow(radius: 3)
                            .onTapGesture(perform: {
                                isEditing = true
                            })
                            .animation(.default)
                    }
                    .background(Color.white)
                    .cornerRadius(10)
                    .shadow(radius: 5)
                    Text("Jenis Kelamin")
                    HStack {
                        TextField("", text: $username)
                            .padding()
                            .background(Color.white)
                            .cornerRadius(5)
                            .shadow(radius: 3)
                            .onTapGesture(perform: {
                                isEditing = true
                            })
                            .animation(.default)
                        Image(systemName: "chevron.down")
                            .padding()
                    }
                    .background(Color.white)
                    .cornerRadius(10)
                    .shadow(radius: 5)
                }
                Text("Instagram")
                HStack {
                    TextField("", text: $username)
                        .padding()
                        .background(Color.white)
                        .cornerRadius(5)
                        .shadow(radius: 3)
                        .onTapGesture(perform: {
                            isEditing = true
                        })
                        .animation(.default)
                }
                .background(Color.white)
                .cornerRadius(10)
                .shadow(radius: 5)
                Text("No. HP")
                HStack {
                    TextField("", text: $username)
                        .padding()
                        .background(Color.white)
                        .cornerRadius(5)
                        .shadow(radius: 3)
                        .onTapGesture(perform: {
                            isEditing = true
                        })
                        .animation(.default)
                }
                .background(Color.white)
                .cornerRadius(10)
                .shadow(radius: 5)
            }
            .padding(.horizontal)
            .padding(.top, 10)
            GeometryReader { geo in
                Button(action: {
                    //
                }, label: {
                    Text("SIMPAN")
                        .bold()
                        .font(.title3)
                        .foregroundColor(.white)
                })
                .frame(width: geo.size.width / 1.11)
                .padding()
                .background(Color.accentColor)
                .cornerRadius(25)
                .shadow(radius: 5)
            }
            .padding()
            .padding(.bottom, 50)
        }
        .navigationBarTitle("EDIT PROFILE", displayMode: .inline)
    }
}

struct EditProfileView_Previews: PreviewProvider {
    static var previews: some View {
        EditProfileView()
    }
}
