//
//  EditCompleteProfileView.swift
//  Test
//
//  Created by Hera on 16/12/20.
//

import SwiftUI

struct EditCompleteProfileView: View {
    var body: some View {
        NavigationView {
            GeometryReader { geo in
                VStack(alignment: .center) {
                    Text("COMPLETE YOUR PROFILE")
                        .bold()
                        .font(.title3)
                    HStack {
                        Image("complete_profile")
                            .resizable()
                            .scaledToFit()
                            .frame(width: 50, height: 50)
                        Text("Hanya butuh 1 menit untuk lengkapi profile anda dan dapatkan point 5.000 pts")
                            .font(.footnote)
                            .lineLimit(2)
                    }
                    
                    Button(action: {
                        //
                    }, label: {
                        NavigationLink(destination: EditProfileView()) {
                            Text("GO")
                                .foregroundColor(.white)
                                .bold()
                        }
                    })
                    .frame(width: 150, height: 10)
                    .padding()
                    .background(Color.accentColor)
                    .cornerRadius(10)
                    
                }
                .frame(width: geo.size.width / 1.1)
                .padding()
                .background(Color.white)
                .cornerRadius(15)
                .shadow(radius: 5)
            }
            .padding(.top, 5)
            .padding(.horizontal, 10)
            .navigationBarHidden(true)
        }
    }
}

struct EditCompleteProfileView_Previews: PreviewProvider {
    static var previews: some View {
        EditCompleteProfileView()
    }
}
