//
//  Edit Profile.swift
//  Test
//
//  Created by Hera on 07/12/20.
//

import SwiftUI

struct EditProfile: View {
    var body: some View {
        NavigationView {
            GeometryReader { geo in
                ScrollView(.vertical, showsIndicators: false) {
                    VStack(alignment: .leading, spacing: 20) {
                        HStack {
                            Text("Nama")
                                .bold()
                            Text("Wanda Shalihat")
                                .padding(.leading)
                        }
                        HStack {
                            Text("Email")
                                .bold()
                            Text("wandashalihat265@gmail.com")
                                .padding(.leading)
                            
                            Spacer()
                            
//                            Image(systemName: "checkmark.seal.fill")
//                                .foregroundColor(/*@START_MENU_TOKEN@*/.blue/*@END_MENU_TOKEN@*/)
//                                .frame(width: 20, height: 20)
                            
                            Image(systemName: "xmark.seal.fill")
                                .foregroundColor(.red)
                                .frame(width: 20, height: 20)
                        }
                        HStack {
                            Text("No. HP")
                                .bold()
                            Text("+6281224715419")
                            
                            Spacer()
                            
//                            Image(systemName: "checkmark.seal.fill")
//                                .foregroundColor(/*@START_MENU_TOKEN@*/.blue/*@END_MENU_TOKEN@*/)
//                                .frame(width: 20, height: 20)
                            Image(systemName: "xmark.seal.fill")
                                .foregroundColor(.red)
                                .frame(width: 20, height: 20)
                        }
                        Button(action: {
                            //
                        }, label: {
                            NavigationLink(destination: EditProfileView()) {
                                Text("EDIT PROFILE")
                                    .bold()
                                    .font(.title3)
                                    .foregroundColor(.white)
                            }
                        })
                        .frame(width: geo.size.width / 1.16)
                        .padding()
                        .background(Color.accentColor)
                        .cornerRadius(25)
                        .shadow(radius: 5)
                        .buttonStyle(PlainButtonStyle())
                    }
                    .padding(.horizontal, 10)
                    
                    Spacer().padding()
                    
                    HStack {
                        Text("Untuk mengirim ulang verifikasi email")
                        Text("Klik di sini")
                            .bold()
                            .font(.system(size: 17))
                        
                    }
                    .padding(.trailing)
                    .padding(.leading)
                    .font(.system(size: 15))
                    .foregroundColor(.white)
                    .background(Color.red)
                    
                    Spacer()
                    
                    Text("_ _ _ _ _ _")
                    Text("Anda dapat mengirim verifikasi lagi pada 05.00")
                        .font(.footnote)
                    
                    Spacer()
                    
                    HStack {
                        Text("Reset")
                        Spacer()
                        Text("Kirim")
                    }
                    .padding(.horizontal, 10)
                    
                    Spacer()
                    
                    VStack(alignment: .leading, spacing: 15) {
                        
                        Spacer()
                        
                        Group {
                            // MARK: - MY PRODUCTS
                            HStack(spacing: 20) {
                                Image(systemName: "text.badge.checkmark")
                                    .resizable()
                                    .scaledToFit()
                                    .frame(width: 25, height: 25, alignment: .center)
                                NavigationLink(destination: NotificationView()) {
                                    Text("My Products")
                                }
                                .foregroundColor(.black)
                                
                            } //: HSTACK
                            
                            Divider()
                            // MARK: - FAQ
                            HStack(spacing: 20) {
                                Image(systemName: "questionmark.circle")
                                    .resizable()
                                    .scaledToFit()
                                    .frame(width: 25, height: 25, alignment: .center)
                                Text("FAQ")
                            } //: HSTACK
                            
                            Divider()
                        } //: GROUP
                        
                        Group {
                            // MARK: - CHANGE LANGUAGE
                            HStack(spacing: 20) {
                                Image(systemName: "globe")
                                    .resizable()
                                    .scaledToFit()
                                    .frame(width: 25, height: 25, alignment: .center)
                                Text("Change Language")
                            } //: HSTACK
                            
                            Divider()
                            // MARK: - TERM OF SERVICES
                            HStack(spacing: 20) {
                                Image(systemName: "info.circle")
                                    .resizable()
                                    .scaledToFit()
                                    .frame(width: 25, height: 25, alignment: .center)
                                Text("Term Of Services")
                            } //: HSTACK
                            
                            Divider()
                        } //: GROUP
                        
                        Group {
                            // MARK: - PRIVACY POLICY
                            HStack(spacing: 20) {
                                Image(systemName: "lock.shield")
                                    .resizable()
                                    .scaledToFit()
                                    .frame(width: 25, height: 25, alignment: .center)
                                Text("Privacy Policy")
                            } //: HSTACK
                            
                            Divider()
                            // MARK: - RATE APP
                            HStack(spacing: 20) {
                                Image(systemName: "star")
                                    .resizable()
                                    .scaledToFit()
                                    .frame(width: 25, height: 25, alignment: .center)
                                Text("Rate Authenctic Guard App")
                            } //: HSTACK
                            
                            Divider()
                        }
                        Button(action: {
                            //
                        }, label: {
                            Text("LOG OUT")
                                .bold()
                                .font(.title3)
                                .foregroundColor(.white)
                        })
                        .frame(width: geo.size.width / 1.16)
                        .padding()
                        .background(Color.accentColor)
                        .cornerRadius(25)
                        .shadow(radius: 5)
                        .buttonStyle(PlainButtonStyle())
                    }
                    .padding(.horizontal, 10)
                    .navigationBarHidden(true)
                }
            }
        }
        .navigationBarHidden(true)
    }
}

struct EditProfilePreviews: PreviewProvider {
    static var previews: some View {
        EditProfile()
    }
}
