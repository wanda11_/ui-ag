//
//  Detail.swift
//  Test
//
//  Created by Hera on 14/12/20.
//

import SwiftUI

struct Detail: View {
    var body: some View {
        GeometryReader { geo in
            VStack(alignment: .leading) {
                Text("Status")
                    .foregroundColor(.gray)
                    .padding(.horizontal)
                Text("The transaction is successful")
                    .foregroundColor(.green)
                    .bold()
                    .padding(.horizontal)
                Divider()
                HStack {
                    Text("Transaction Date")
                        .foregroundColor(.gray)
                    Spacer()
                    Text("09 Jul 2020 15:37")
                }
                .padding(.horizontal)
                HStack {
                    Text("Product Category")
                        .foregroundColor(.gray)
                    Spacer()
                    Text("Point")
                }
                .padding(.horizontal)
                Divider()
                Text("Purchase Detail")
                    .bold()
                    .padding(.horizontal)
                Text("Berhasil TOP UP Point Sebanyak 10960 Point.")
                    .foregroundColor(.gray)
                    .padding(.horizontal)
                    .fixedSize()
                Divider()
            }
            .padding(.top, 10)
            .navigationBarTitle("Notification", displayMode: .inline)
        }
    }
}

struct Detail_Previews: PreviewProvider {
    static var previews: some View {
        Detail()
    }
}
