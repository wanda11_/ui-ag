//
//  NotifView.swift
//  Test
//
//  Created by Hera on 14/12/20.
//

import SwiftUI

struct notif {
    var id: Int
    let image, text1, text2: String
}

struct NotifView: View {
    
    let not: [notif] = [
        //        notif(id: 0, image: "mail_read", text1: "Point", text2: "Berhasil TOP UP Point Sebanyak 10960 Point."),
        //        notif(id: 1, image: "mail_read", text1: "Point", text2: "Berhasil TOP UP Point Sebanyak 10960 Point."),
        //        notif(id: 2, image: "mail_read", text1: "Point", text2: "Berhasil TOP UP Point Sebanyak 10960 Point."),
        //        notif(id: 3, image: "mail_read", text1: "Point", text2: "Berhasil TOP UP Point Sebanyak 10960 Point.")
        notif(id: 0, image: "mail_unread", text1: "Point", text2: "Berhasil TOP UP Point Sebanyak 10960 Point."),
        notif(id: 1, image: "mail_unread", text1: "Point", text2: "Berhasil TOP UP Point Sebanyak 10960 Point."),
        notif(id: 2, image: "mail_unread", text1: "Point", text2: "Berhasil TOP UP Point Sebanyak 10960 Point."),
        notif(id: 3, image: "mail_unread", text1: "Point", text2: "Berhasil TOP UP Point Sebanyak 10960 Point.")
    ]
    
    let columns = [
        GridItem(.flexible())
    ]
    
    var body: some View {
        NavigationView {
            GeometryReader { geo in
                ScrollView(.vertical, showsIndicators: false) {
                    LazyVGrid(columns: columns) {
                        ForEach(not, id: \.id) { no in
                            noti(n: no)
                        }
                    }
                }
            }
            .navigationBarHidden(true)
        }
    }
}
struct noti:View {
    
    let n: notif
    
    var body: some View {
        NavigationLink(destination: Detail()) {
            VStack {
                HStack {
                    Spacer()
                    Image(n.image)
                        .resizable()
                        .frame(width: 40, height: 40)
                    
                    Spacer()
                    
                    VStack(alignment: .leading, spacing: 10) {
                        Text(n.text1)
                            .bold()
                        Text(n.text2)
                            .font(.footnote)
                            .fixedSize()
                    }
                    .padding(.trailing)
                }
                .foregroundColor(.black)
                Divider()
            }
        }
    }
}

struct NotifView_Previews: PreviewProvider {
    static var previews: some View {
        NotifView()
    }
}
