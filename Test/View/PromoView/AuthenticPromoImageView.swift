//
//  AuthenticPromoView.swift
//  Test
//
//  Created by Hera on 03/12/20.
//

import SwiftUI

struct High {
    var id: Int
    let image: String
}

struct AuthenticPromoImageView: View {
    
    let higher: [High] = [
        High(id: 0, image: "0"),
        High(id: 1, image: "1"),
        High(id: 2, image: "2"),
        High(id: 3, image: "3")
    ]
    
    var body: some View {
        NavigationView {
            ScrollView(.horizontal, showsIndicators: false) {
                HStack(spacing: 20) {
                    ForEach(higher, id: \.id) { hi in
                        HighView(high: hi)
                    }
                }
                .padding(.leading, 10)
                .padding(.trailing, 10)
            }
            .navigationBarHidden(true)
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct HighView: View {
    
    let high: High
    
    var body: some View {
        NavigationLink(destination: DetailView()) {
            HStack {
                Image(high.image)
                    .resizable()
                    .scaledToFill()
                    .frame(width: 350, height: 225)
                    .cornerRadius(10)
            }
        }
    }
}

struct AuthenticPromoView_Previews: PreviewProvider {
    static var previews: some View {
        AuthenticPromoImageView()
    }
}
