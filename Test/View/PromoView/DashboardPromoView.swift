//
//  DashboardPromoView.swift
//  Test
//
//  Created by Hera on 03/12/20.
//

import SwiftUI

struct DashboardPromoView: View {
    var body: some View {
        NavigationView {
            GeometryReader { geo in
                VStack {
                    HStack {
                        Text("Authentic Promo")
                            .bold()
                            .padding(.leading, 10)
                            .font(.title2)
                        
                        Spacer()
                        
                        NavigationLink(destination: PromoGridView()) {
                        Text("See All")
                            .foregroundColor(.accentColor)
                            .bold()
                            .padding(.trailing, 10)
                        }
                    }
                    AuthenticPromoImageView()
                }
                .navigationBarHidden(true)
            }
        }
    }
}

struct DashboardPromoView_Previews: PreviewProvider {
    static var previews: some View {
        DashboardPromoView()
    }
}
