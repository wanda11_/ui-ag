//
//  PromoView.swift
//  Test
//
//  Created by Hera on 03/12/20.
//

import SwiftUI

struct promo {
    var id: Int
    let image1, title, alamat, toko: String
}

struct PromoGridView: View {
    
    let promos: [promo] = [
        promo(id: 0, image1: "1", title: "Summer Sale", alamat: "Jl. Pasirluyu VII No.7 Bandung 40254", toko: "Sabichi"),
        promo(id: 1, image1: "2", title: "Summer Sale", alamat: "Jl. Pasirluyu VII No.7 Bandung 40254", toko: "Sabichi"),
        promo(id: 2, image1: "3", title: "Summer Sale", alamat: "Jl. Pasirluyu VII No.7 Bandung 40254", toko: "Sabichi"),
        promo(id: 3, image1: "4", title: "Summer Sale", alamat: "Jl. Pasirluyu VII No.7 Bandung 40254", toko: "Sabichi"),
    ]
    
    let columns = [
        GridItem(.flexible())
    ]
    
    var body: some View {
        NavigationView {
            ScrollView(.vertical, showsIndicators: false) {
                LazyVGrid(columns: columns) {
                    ForEach(promos, id: \.id) { pro in
                        Promo(promos: pro)
                    }
                }
            }
            .navigationBarHidden(true)
        }
        .navigationBarTitle("Authentic Promo", displayMode: .inline)
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct Promo: View {
    
    let promos: promo
    
    var body: some View {
        NavigationLink(destination: DetailView()) {
            HStack(alignment: .center) {
                Image(promos.image1)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .cornerRadius(10)
                    .frame(width: 150)
                    .padding(.all, 10)
                
                
                VStack(alignment: .leading, spacing: 5) {
                    Text(promos.title)
                    Text(promos.alamat)
                    Text(promos.toko)
                }
                .padding(.trailing, 10)
                Spacer()
            }
            .frame(maxWidth: .infinity, alignment: .center)
            .background(Color(.sRGB, red: 150/255, green: 150/255, blue: 150/255, opacity: 0.1))
            .cornerRadius(10)
            .padding(.all, 10)
        }
        .foregroundColor(.black)
    }
}

struct PromoView_Previews: PreviewProvider {
    static var previews: some View {
        PromoGridView()
    }
}
