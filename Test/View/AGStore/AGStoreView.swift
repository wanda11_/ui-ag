//
//  AGStoreView.swift
//  Test
//
//  Created by Hera on 03/12/20.
//

import SwiftUI

struct AGStoreView: View {
    var body: some View {
        AGStoreGridView()
            .navigationBarTitle("AG Store", displayMode: .inline)
    }
}

struct AGStoreView_Previews: PreviewProvider {
    static var previews: some View {
        AGStoreView()
    }
}
