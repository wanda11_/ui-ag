//
//  DashboardAGStoreView.swift
//  Test
//
//  Created by Hera on 03/12/20.
//

import SwiftUI

struct DashboardAGStoreView: View {
    var body: some View {
        VStack(alignment: .leading) {
            Text("AG Store")
                .bold()
                .font(.title2)
                .padding(.leading, 10)
            NavigationView {
                GeometryReader { geo in
                    NavigationLink(destination: AGStoreView()) {
                        ZStack(alignment: .bottomLeading) {
                            Image("2")
                                .resizable()
                                .clipped()
                            VStack {
                                Text("SABICHI")
                                    .padding()
                                    .foregroundColor(.white)
                            }
                            .edgesIgnoringSafeArea(.all)
                            .frame(width: geo.size.width, height: 50, alignment: .bottomLeading)
                            .background(BlurView(style: .systemUltraThinMaterial))
                            .fixedSize()
                        }
                    }
                }
                .navigationBarHidden(true)
            }
        }
    }
}

struct DashboardAGStoreView_Previews: PreviewProvider {
    static var previews: some View {
        DashboardAGStoreView()
    }
}
