//
//  AGStoreGridView.swift
//  Test
//
//  Created by Hera on 03/12/20.
//

import SwiftUI

struct store {
    var id: Int
    var title1, alamat1, image1: String
}

struct AGStoreGridView: View {
    
    let storeAG: [store] = [
        store(id: 0, title1: "SABICHI", alamat1: "Jl. Pasirluyu VII No.7 Bandung 40254", image1: "0"),
        store(id: 1, title1: "MORUKA", alamat1: "Jl. Pasirluyu VII No.7 Bandung 40254", image1: "1"),
        store(id: 2, title1: "MOWIC", alamat1: "Jl. Pasirluyu VII No.7 Bandung 40254", image1: "2"),
        store(id: 3, title1: "BYHERS", alamat1: "Jl. Pasirluyu VII No.7 Bandung 40254", image1: "3"),
        store(id: 4, title1: "DEENAY", alamat1: "Jl. Pasirluyu VII No.7 Bandung 40254", image1: "4"),
        store(id: 5, title1: "SABICHI", alamat1: "Jl. Pasirluyu VII No.7 Bandung 40254", image1: "5"),
    ]
    
    let columns = [
        GridItem(.flexible()),
        GridItem(.flexible())
    ]
    
    var body: some View {
        ScrollView {
            LazyVGrid(columns: columns, spacing: 15) {
                ForEach(storeAG, id: \.id) { ag in
                    AGSView(ags: ag)
                        .padding(.top, 10)
                }
            }
        }
    }
}


struct AGSView: View {
    
    let ags: store
    
    var body: some View {
        
        GeometryReader { geo in
            ZStack(alignment: .bottom) {
                VStack(alignment: .center, spacing: 10) {
                    Text(ags.title1)
                        .bold()
                        .font(.title2)
                        .padding(.top, 5)
                    HStack {
                        Spacer()
                        Text(ags.alamat1)
                        Spacer()
                    }
                    Image(ags.image1)
                        .resizable()
                        .clipped()
                        .frame(width: 100, height: 100)
                    
                    Button(action: {
                        //
                    }, label: {
                        Text("Get Locations")
                            .foregroundColor(.white)
                    })
                    .padding()
                    .background(Color.accentColor)
                    .cornerRadius(25)
                    .shadow(radius: 5)
                }
                .frame(width: 175, height: 275)
            }
            .frame(width: geo.size.width, height: 275)
            .background(Color.white)
            .cornerRadius(25)
            .shadow(radius: 5)
        }
        .frame(height: 275)
        .padding(.horizontal, 10)
    }
}

struct AGStoreGridView_Previews: PreviewProvider {
    static var previews: some View {
        AGStoreGridView()
    }
}
