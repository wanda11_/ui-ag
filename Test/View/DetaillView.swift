//
//  DetaillView.swift
//  Test
//
//  Created by Hera on 07/12/20.
//

import SwiftUI

struct DetaillView: View {
    var body: some View {
        NavigationView {
            GeometryReader { geo in
                HStack {
                    Spacer()
                    Image("0")
                        .resizable()
                        .clipped()
                        .clipShape(Circle())
                        .frame(width: 120, height: 120)
                    Spacer()
                    Text("Authentic Guards Technology")
                        .bold()
                    Spacer()
                    VStack {
                        Button(action: {
                            //
                        }, label: {
                            HStack {
                                Spacer()
                                Image(systemName: "plus")
                                    .resizable()
                                    .clipped()
                                    .foregroundColor(.white)
                                    .frame(width: 15, height: 15)
                                Spacer()
                                Text("FOLLOW")
                                    .bold()
                                    .font(.title3)
                                    .foregroundColor(.white)
                                    .fixedSize()
                                Spacer()
                            }
                        })
                        .frame(width: 125, height: 50)
                        .background(Color.accentColor)
                        .cornerRadius(10)
                        .shadow(radius: 5)
                        
                        Button(action: {
                            //
                        }, label: {
                            HStack {
                                Spacer()
                                Image(systemName: "text.bubble.fill")
                                    .resizable()
                                    .clipped()
                                    .foregroundColor(.white)
                                    .frame(width: 20, height: 20)
                                Spacer()
                                Text("CHAT")
                                    .bold()
                                    .font(.title3)
                                    .foregroundColor(.white)
                                    .fixedSize()
                                Spacer()
                            }
                        })
                        .frame(width: 125, height: 50)
                        .background(Color.accentColor)
                        .cornerRadius(10)
                        .shadow(radius: 5)
                    }
                    Spacer()
                }
            }
            .navigationBarHidden(true)
        }
        .padding(.top)
    }
}

struct DetaillView_Previews: PreviewProvider {
    static var previews: some View {
        DetaillView()
    }
}
