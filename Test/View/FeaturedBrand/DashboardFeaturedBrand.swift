//
//  DashboardFeaturedBrand.swift
//  Test
//
//  Created by Hera on 03/12/20.
//

import SwiftUI

struct DashboardFeaturedBrand: View {
    var body: some View {
        NavigationView {
            GeometryReader { geo in
                VStack {
                    HStack {
                        Text("Featured Brand")
                            .bold()
                            .padding(.leading, 10)
                            .font(.title2)
                        
                        Spacer()
                        
                        NavigationLink(destination: DetailFeaturedBrandView()) {
                            Text("See All")
                                .foregroundColor(.accentColor)
                                .bold()
                                .padding(.trailing, 10)
                        }
                    }
                    
                    FeaturedBrandView()
                        .padding(.horizontal, 10)
                }
                .navigationBarHidden(true)
            }
        }
    }
}

struct DashboardFeaturedBrand_Previews: PreviewProvider {
    static var previews: some View {
        DashboardFeaturedBrand()
    }
}
