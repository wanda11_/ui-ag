//
//  DetailFeaturedBrandView.swift
//  Test
//
//  Created by Hera on 03/12/20.
//

import SwiftUI

struct detail {
    var id: Int
    let image1, title, alamat, toko: String
}

struct DetailFeaturedBrandView: View {
    
    let details: [detail] = [
        detail(id: 0, image1: "1", title: "Summer Sale", alamat: "Jl. Pasirluyu VII No.7 Bandung 40254", toko: "Sabichi"),
        detail(id: 1, image1: "2", title: "Summer Sale", alamat: "Jl. Pasirluyu VII No.7 Bandung 40254", toko: "Sabichi"),
        detail(id: 2, image1: "3", title: "Summer Sale", alamat: "Jl. Pasirluyu VII No.7 Bandung 40254", toko: "Sabichi"),
        detail(id: 3, image1: "4", title: "Summer Sale", alamat: "Jl. Pasirluyu VII No.7 Bandung 40254", toko: "Sabichi"),
    ]
    
    let columns = [
        GridItem(.flexible())
    ]
    
    var body: some View {
        NavigationView {
            ScrollView(.vertical, showsIndicators: false) {
                LazyVGrid(columns: columns) {
                    ForEach(details, id: \.id) { deta in
                        detailview(det: deta)
                    }
                }
            }
            .navigationBarHidden(true)
        }
        .navigationBarTitle("Featured Brand", displayMode: .inline)
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct detailview: View {
    
    let det: detail
    
    var body: some View {
        NavigationLink(destination: DetaillView()) {
            
            HStack(alignment: .center) {
                Image(det.image1)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .cornerRadius(10)
                    .frame(width: 150)
                    .padding(.all, 10)
                
                
                VStack(alignment: .leading, spacing: 5) {
                    Text(det.title)
                    Text(det.alamat)
                    Text(det.toko)
                }
                .padding(.trailing, 10)
                Spacer()
            }
            .frame(maxWidth: .infinity, alignment: .center)
            .background(Color(.sRGB, red: 150/255, green: 150/255, blue: 150/255, opacity: 0.1))
            .cornerRadius(10)
            .padding(.all, 10)
        }
        .foregroundColor(.black)
    }
}

struct DetailFeaturedBrandView_Previews: PreviewProvider {
    static var previews: some View {
        DetailFeaturedBrandView()
    }
}
