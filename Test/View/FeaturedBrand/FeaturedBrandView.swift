//
//  FeaturedBrandView.swift
//  Test
//
//  Created by Hera on 03/12/20.
//

import SwiftUI

struct feature {
    var id: Int
    var image: String
}

struct FeaturedBrandView: View {
    
    let feat: [feature] = [
        feature(id: 0, image: "0"),
        feature(id: 1, image: "1"),
        feature(id: 2, image: "2"),
        feature(id: 3, image: "3"),
        feature(id: 4, image: "4"),
        feature(id: 5, image: "5"),
    ]
    
    let columns = [
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible())
    ]
    
    var body: some View {
        NavigationView {
            LazyVGrid(columns: columns, spacing: 10) {
                ForEach(feat, id: \.id) { featu in
                    featured(features: featu)
                }
            }
            .navigationBarHidden(true)
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct featured: View {
    let features: feature
    
    var body: some View {
        NavigationLink(destination: DetaillView()) {
            Image(features.image)
                .resizable()
                .clipped()
                .cornerRadius(15)
                .frame(width: 115, height: 115)
        }
    }
}

struct FeaturedBrandView_Previews: PreviewProvider {
    static var previews: some View {
        FeaturedBrandView()
    }
}
