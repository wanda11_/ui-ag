//
//  ListrikView.swift
//  Test
//
//  Created by Hera on 15/12/20.
//

import SwiftUI

struct ListrikView: View {
    
    @State var page: String = "prabayar"
    
    var body: some View {
        VStack {
            Picker("Transaksi", selection: $page) {
                Text("PRABAYAR (TOKEN)").tag("prabayar")
                Text("PASCABAYAR").tag("pascabayar")
            }
            .pickerStyle(SegmentedPickerStyle())
            .padding(.top, 10)
            if page == "prabayar" {
                PrabayarView()
            } else {
                PascabayarView()
            }
        }
        .navigationBarTitle("LISTRIK", displayMode: .inline)
    }
}

struct ListrikView_Previews: PreviewProvider {
    static var previews: some View {
        ListrikView()
    }
}
