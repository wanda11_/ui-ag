//
//  PLNView.swift
//  Test
//
//  Created by Hera on 03/12/20.
//

import SwiftUI

struct PrabayarView: View {
    
    @State var username: String = ""
    
    @State private var isEditing = true
    
    var body: some View {
        VStack(alignment: .center) {
            
            Text("Masukkan Nomor Pelanggan")
                .bold()
                .padding(.leading, 20)
            
            HStack {
                TextField("Contoh: 12345678", text: $username)
                    .padding()
                    .background(Color.white)
                    .cornerRadius(5)
                    .shadow(radius: 5)
                    .onTapGesture(perform: {
                        isEditing = true
                    })
                    .animation(.default)
            }
            .background(Color.white)
            .cornerRadius(5)
            .shadow(radius: 5)
            .padding()
            
            Text("Anda belum memiliki riwayat pembelian")
            
            GeometryReader { geo in
                Button(action: {
                    //
                }, label: {
                    Text("LANJUTKAN")
                        .bold()
                        .font(.title3)
                        .foregroundColor(.white)
                })
                .frame(width: geo.size.width / 1.09)
                .padding()
                .background(Color.accentColor)
                .cornerRadius(25)
                .shadow(radius: 5)
            }
            .padding(.horizontal, 5)
        }
        .padding(.top, 5)
    }
}

struct PLNView_Previews: PreviewProvider {
    static var previews: some View {
        PrabayarView()
    }
}
