//
//  PascabayarView.swift
//  Test
//
//  Created by Hera on 03/12/20.
//

import SwiftUI

struct PascabayarView: View {
    
    @State var username: String = ""
    
    @State private var isEditing = true
    
    var body: some View {
        GeometryReader { geo in
            VStack(alignment: .leading, spacing: 5) {
                Group {
                    Text("No. Meter/ID Pelanggan")
                        .bold()
                        .fixedSize()
                    
                    Spacer()
                    
                    TextField("112233445566", text: $username)
                        .padding()
                        .background(Color.white)
                        .cornerRadius(5)
                        .shadow(radius: 5)
                        .onTapGesture(perform: {
                            isEditing = true
                        })
                        .animation(.default)
                }
                
                Text("Nama Pelanggan")
                    .padding(.top, 10)
                HStack {
                    Text("Toal Tagihan")
                    Spacer()
                    Text("1 Bulan")
                    Spacer()
                }
                Text("Periode Tagihan")
                Text("kW/h rate")
                Text("Total Tagihan")
                    .bold()
                    .padding(.top, 30)
                Text("Tagihan")
                Text("Biaya Admin")
                
                GeometryReader { geo in
                    Button(action: {
                        //
                    }, label: {
                        Text("LIHAT TAGIHAN")
                            .bold()
                            .font(.title3)
                            .foregroundColor(.white)
                    })
                    .frame(width: geo.size.width / 1.09)
                    .padding()
                    .background(Color.accentColor)
                    .cornerRadius(25)
                    .shadow(radius: 5)
                }
                .padding(.top, 20)
            }
        }
        .padding()
    }
}

struct PascabayarView_Previews: PreviewProvider {
    static var previews: some View {
        PascabayarView()
    }
}
