//
//  PulsaView.swift
//  Test
//
//  Created by Hera on 03/12/20.
//

import SwiftUI

struct PulsaView: View {
    
    @State var username: String = ""
    
    @State private var isEditing = true
    
    var body: some View {
        NavigationView {
            GeometryReader { geo in
                VStack(alignment: .center, spacing: 20) {
                    Text("Masukkan Nomor HP")
                        .bold()
                        .padding(.leading, 20)
                    
                    HStack {
                        TextField("Contoh: 0812345678", text: $username)
                            .padding()
                            .background(Color.white)
                            .cornerRadius(5)
                            .shadow(radius: 5)
                            .onTapGesture(perform: {
                                isEditing = true
                            })
                            .animation(.default)
                        Image(systemName: "rectangle.stack.person.crop")
                            .resizable()
                            .frame(width: 50, height: 50)
                            .foregroundColor(Color.gray)
                            .padding(.trailing, 10)
                    }
                    .background(Color.white)
                    .cornerRadius(5)
                    .shadow(radius: 5)
                    
                    Text("Anda belum memiliki riwayat pembelian")
                    
                    Button(action: {
                        //
                    }, label: {
                        NavigationLink(destination: DetailPulsaView()) {
                        Text("LANJUTKAN")
                            .bold()
                            .font(.title3)
                            .foregroundColor(.white)
                        }
                    })
                    .frame(width: geo.size.width / 1.09)
                    .padding()
                    .background(Color.accentColor)
                    .cornerRadius(25)
                    .shadow(radius: 5)
                    
                    HStack {
                        Spacer()
                        Image("ppob - pulsa baru")
                            .resizable()
                            .scaledToFit()
                            .frame(width: 50, height: 50)
                        Spacer()
                        VStack(alignment: .leading, spacing: 5) {
                            Text("16 Jul 2020 15:42")
                            Text("Rp. 6,000 Telkomsel")
                            Text("+6281224998446")
                        }
                        .lineLimit(3)
                        .font(.footnote)
                        
                        Spacer()
                        Button(action: {
                            //
                        }, label: {
                            Text("BELI LAGI")
                                .foregroundColor(.black)
                        })
                        .padding()
                        .background(Color.white)
                        .cornerRadius(25)
                        .shadow(radius: 5)
                        Spacer()
                    }
                }
            }
            .navigationBarHidden(true)
            .padding()
        }
        .navigationBarTitle("PULSA", displayMode: .inline)
    }
}

struct PulsaView_Previews: PreviewProvider {
    static var previews: some View {
        PulsaView()
    }
}
