//
//  DetailPulsaView.swift
//  Test
//
//  Created by Hera on 15/12/20.
//

import SwiftUI

struct DetailPulsaView: View {
    var body: some View {
        GeometryReader { geo in
            VStack(alignment: .leading, spacing: 10) {
                HStack {
                    Text("No. Pelanggan")
                        .bold()
                    Spacer()
                    Text("AGT Poin")
                        .bold()
                }
                HStack {
                    Text("081224715419")
                    Image("simpati")
                        .resizable()
                        .scaledToFit()
                        .frame(width: 70, height: 30)
                    Spacer()
                    Text("0 pts")
                }
                .foregroundColor(.gray)
                DetailPulsaGridView()
            }
            .padding(.horizontal, 10)
            .navigationBarHidden(true)
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct DetailPulsaView_Previews: PreviewProvider {
    static var previews: some View {
        DetailPulsaView()
    }
}
