//
//  DetailInfoPulsaView.swift
//  Test
//
//  Created by Hera on 15/12/20.
//

import SwiftUI

struct DetailInfoPulsaView: View {
    var body: some View {
        GeometryReader { geo in
            VStack(alignment: .center, spacing: 10) {
                // MARK: INFO PELANGGAN
                Text("Informasi Pelanggan")
                    .font(.title)
                HStack {
                    Text("Telkomsel")
                    Spacer()
                    Text("Pembelian Pulsa")
                }
                HStack {
                    Text("Nomor Ponsel")
                    Spacer()
                    Text("081224715419")
                }
                
                Spacer()
                
                // MARK: DETAIL PEMBAYARAN
                Group {
                    Text("Detail Pembayaran")
                        .font(.title)
                    HStack {
                        Text("Nominal Voucher")
                        Spacer()
                        Text("100,000")
                    }
                    HStack {
                        Text("Harga Voucher")
                        Spacer()
                        Text("101,500")
                    }
                    HStack {
                        Text("Biaya Admin")
                        Spacer()
                        Text("0")
                    }
                    HStack {
                        Text("Point Saya")
                        Spacer()
                        Text("0 pts")
                    }
                    HStack {
                        Text("Diskon")
                        Spacer()
                        Text("0")
                    }
                    
                    Spacer()
                    
                    // MARK: TOTAL PEMBAYARAN
                    HStack {
                        Text("Total Pembayaran")
                        Spacer()
                        Text("101,500 pts")
                    }
                    
                    Spacer()
                }
                
                Group {
                    Button(action: {
                        //
                    }, label: {
                        Text("BAYAR")
                            .foregroundColor(.white)
                            .font(.title2)
                            .bold()
                    })
                    .frame(width: 300, height: 25)
                    .padding()
                    .background(Color.accentColor)
                    .cornerRadius(25)
                    .shadow(radius: 5)
                    .buttonStyle(PlainButtonStyle())
                }
                
                Spacer()
            }
            .navigationBarHidden(true)
            .padding()
            .frame(width: geo.size.width)
        }
    }
}

struct DetailInfoPulsaView_Previews: PreviewProvider {
    static var previews: some View {
        DetailInfoPulsaView()
    }
}
