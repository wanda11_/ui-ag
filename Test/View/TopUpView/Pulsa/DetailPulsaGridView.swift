//
//  DetailPulsaGridView.swift
//  Test
//
//  Created by Hera on 15/12/20.
//

import SwiftUI

struct v {
    var id: Int
    let nominal, harganom: String
}

struct DetailPulsaGridView: View {
    
    let puview: [v] = [
        v(id: 0, nominal: "5,000", harganom: "Harga: Rp. 6,500"),
        v(id: 1, nominal: "10,000", harganom: "Harga: Rp. 11,500"),
        v(id: 2, nominal: "15,000", harganom: "Harga: Rp. 16,500"),
        v(id: 3, nominal: "20,000", harganom: "Harga: Rp. 21,500"),
        v(id: 4, nominal: "25,000", harganom: "Harga: Rp. 26,500"),
        v(id: 5, nominal: "30,000", harganom: "Harga: Rp. 31,500"),
        v(id: 6, nominal: "40,000", harganom: "Harga: Rp. 41,500"),
        v(id: 7, nominal: "50,000", harganom: "Harga: Rp. 51,500"),
        v(id: 8, nominal: "75,000", harganom: "Harga: Rp. 76,500"),
        v(id: 9, nominal: "100,000", harganom: "Harga: Rp. 101.500")
    ]
    
    let columns = [
        GridItem(.flexible()),
        GridItem(.flexible())
    ]
    
    var body: some View {
        NavigationView {
            GeometryReader { geo in
                ScrollView(.vertical, showsIndicators: false) {
                    
                    Spacer()
                    
                    LazyVGrid(columns: columns) {
                        ForEach(puview, id: \.id) { pgv in
                            p(pv: pgv)
                                .frame(width: geo.size.width)
                        }
                    }
                }
            }
            .navigationBarHidden(true)
        }
    }
}

struct p: View {
    
    var pv: v
    
    var body: some View {
        NavigationLink(destination: DetailInfoPulsaView()) {
            VStack(alignment: .leading, spacing: 5) {
                Text(pv.nominal)
                    .font(.title2)
                    .foregroundColor(.black)
                    .bold()
                Text(pv.harganom)
            }
            .padding()
            .background(Color.white)
            .cornerRadius(10)
            .shadow(radius: 5)
        }
        .foregroundColor(.black)
    }
}

struct DetailPulsaGridView_Previews: PreviewProvider {
    static var previews: some View {
        DetailPulsaGridView()
    }
}
