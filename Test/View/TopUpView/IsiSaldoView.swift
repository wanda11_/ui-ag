//
//  IsiSaldoView.swift
//  Test
//
//  Created by Hera on 15/12/20.
//

import SwiftUI

struct IsiSaldoView: View {
    var body: some View {
        NavigationView {
            GeometryReader { geo in
                VStack(alignment: .trailing, spacing: 10) {
                    Text("Lihat Semua")
                        .bold()
                        .foregroundColor(.accentColor)
                        .padding(.trailing, 20)
                    
                    ScrollView(.horizontal, showsIndicators: false) {
                        HStack {
                            NavigationLink(destination: PulsaView()) {
                                VStack {
                                    Image("ppob - pulsa baru")
                                        .resizable()
                                        .clipped()
                                        .frame(width: 40, height: 40)
                                    Text("PULSA")
                                        .bold()
                                        .font(.title3)
                                        .fixedSize()
                                        .foregroundColor(.black)
                                }
                                .frame(width: 70, height: 70)
                                .padding()
                                .background(Color.white)
                                .cornerRadius(15)
                                .shadow(radius: 5)
                            }
                            .padding(.leading, 10)
                            VStack {
                                Image("ppob - telepon baru")
                                    .resizable()
                                    .clipped()
                                    .frame(width: 40, height: 40)
                                Text("TELEPON")
                                    .bold()
                                    .font(.title3)
                                    .fixedSize()
                                    .foregroundColor(.black)
                            }
                            .frame(width: 70, height: 70)
                            .padding()
                            .background(Color.white)
                            .cornerRadius(15)
                            .shadow(radius: 5)
                            
                            VStack {
                                Image("ppob - internet baru")
                                    .resizable()
                                    .clipped()
                                    .frame(width: 40, height: 40)
                                Text("INTERNET")
                                    .bold()
                                    .font(.title3)
                                    .fixedSize()
                                    .foregroundColor(.black)
                            }
                            .frame(width: 70, height: 70)
                            .padding()
                            .background(Color.white)
                            .cornerRadius(15)
                            .shadow(radius: 5)
                            
                            NavigationLink(destination: ListrikView()) {
                                VStack {
                                    Image("ppob - pln")
                                        .resizable()
                                        .clipped()
                                        .frame(width: 40, height: 40)
                                    Text("PLN")
                                        .bold()
                                        .font(.title3)
                                        .fixedSize()
                                        .foregroundColor(.black)
                                }
                                .frame(width: 70, height: 70)
                                .padding()
                                .background(Color.white)
                                .cornerRadius(15)
                                .shadow(radius: 5)
                            }
                            VStack {
                                Image("ppob - pdam")
                                    .resizable()
                                    .clipped()
                                    .frame(width: 40, height: 40)
                                Text("PDAM")
                                    .bold()
                                    .font(.title3)
                                    .fixedSize()
                                    .foregroundColor(.black)
                            }
                            .frame(width: 70, height: 70)
                            .padding()
                            .background(Color.white)
                            .cornerRadius(15)
                            .shadow(radius: 5)
                            VStack {
                                Image("ppob - bpjs")
                                    .resizable()
                                    .clipped()
                                    .frame(width: 40, height: 40)
                                Text("BPJS")
                                    .bold()
                                    .font(.title3)
                                    .fixedSize()
                                    .foregroundColor(.black)
                            }
                            .frame(width: 70, height: 70)
                            .padding()
                            .background(Color.white)
                            .cornerRadius(15)
                            .shadow(radius: 5)
                            VStack {
                                Image("ppob - kereta")
                                    .resizable()
                                    .clipped()
                                    .frame(width: 40, height: 40)
                                Text("KERETA")
                                    .bold()
                                    .font(.title3)
                                    .fixedSize()
                                    .foregroundColor(.black)
                            }
                            .frame(width: 70, height: 70)
                            .padding()
                            .background(Color.white)
                            .cornerRadius(15)
                            .shadow(radius: 5)
                            .padding(.trailing, 10)
                        }
                        .frame(width: 775, height: 105)
                    }
                }
                .padding(.top, 10)
                .padding(.bottom, 10)
                .background(Color.white)
                .cornerRadius(10)
                .shadow(radius: 5)
            }
            .navigationBarHidden(true)
            .padding(.top, 5)
            .padding(.horizontal, 10)
        }
    }
}

struct IsiSaldoView_Previews: PreviewProvider {
    static var previews: some View {
        IsiSaldoView()
    }
}
