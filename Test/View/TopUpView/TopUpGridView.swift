//
//  TopUpGridView.swift
//  Test
//
//  Created by Hera on 01/12/20.
//

import SwiftUI

struct top {
    var id: Int
    let title, imageURL: String
}

struct TopUpGridView: View {
    
    let topup: [top] = [
        top(id: 0, title: "PULSA", imageURL: "ppob - pulsa baru"),
        top(id: 1, title: "TELEPON", imageURL: "ppob - telepon baru"),
        top(id: 2, title: "INTERNET", imageURL: "ppob - internet baru"),
        top(id: 3, title: "PLN", imageURL: "ppob - pln"),
        top(id: 4, title: "PDAM", imageURL: "ppob - pdam"),
        top(id: 5, title: "BPJS", imageURL: "ppob - bpjs"),
        top(id: 6, title: "KERETA", imageURL: "ppob - kereta"),
        top(id: 7, title: "RIWAYAT", imageURL: "history")
    ]
    
    let columns = [
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible())
    ]
    
    var body: some View {
        GeometryReader { geo in
            ScrollView(.vertical, showsIndicators: false) {
                
                Spacer()
                
                LazyVGrid(columns: columns, spacing: 20) {
                    ForEach(topup, id: \.id) { tp in
                        TopUp(topupv: tp)
                            .frame(width: geo.size.width, height: 110)
                    }
                }
            }
            .navigationBarTitle("All PPOB", displayMode: .inline)
        }
    }
}

struct TopUp: View {
    
    let topupv: top
    
    var body: some View {
        VStack {
            Image(topupv.imageURL)
                .resizable()
                .scaledToFit()
                .frame(width: 60, height: 60)
            Text(topupv.title)
                .font(.title2)
                .foregroundColor(.accentColor)
                .fixedSize()
        }
        .frame(width: 80, height: 80)
        .padding()
        .background(Color.white)
        .cornerRadius(10)
        .shadow(radius: 5)
    }
}

struct TopUpGridView_Previews: PreviewProvider {
    static var previews: some View {
        TopUpGridView()
    }
}
