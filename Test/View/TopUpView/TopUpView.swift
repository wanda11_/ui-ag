//
//  TopUpView.swift
//  Test
//
//  Created by Hera on 27/11/20.
//

import SwiftUI

struct tuv {
    var id: Int
    var image, text: String
}

struct TopUpView: View {
    
    // MARK: - PROPERTIES
    
    let tu: [tuv] = [
        tuv(id: 0, image: "ppob - pulsa baru", text: "PULSA"),
        tuv(id: 1, image: "ppob - telepon baru", text: "TELEPON"),
        tuv(id: 2, image: "ppob - internet baru", text: "INTERNET"),
        tuv(id: 3, image: "ppob - pln", text: "PLN"),
        tuv(id: 4, image: "ppob - pdam", text: "PDAM"),
        tuv(id: 5, image: "ppob - bpjs", text: "BPJS"),
        tuv(id: 6, image: "ppob - kereta", text: "KERETA"),
        tuv(id: 7, image: "history", text: "Riwayat"),
    ]
    
    let columns = [
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible())
    ]
    
    // MARK: - BODY
    
    var body: some View {
            ScrollView(.horizontal, showsIndicators: false) {
                LazyVGrid(columns: columns) {
                    ForEach(tu, id: \.id) { tuview in
                        touv(t: tuview)
                    }
                    .frame(width: 900, height: 100)
                }
            }
            .navigationBarHidden(true)
    }
}

struct touv: View {
    let t: tuv
    
    var body: some View {
        HStack {
            VStack {
                Image(t.image)
                    .resizable()
                    .clipped()
                    .frame(width: 40, height: 40)
                Text(t.text)
                    .bold()
                    .font(.title3)
                    .fixedSize()
            }
            .frame(width: 60, height: 60)
            .padding()
            .background(Color.white)
            .cornerRadius(15)
            .shadow(radius: 5)
        }
    }
}

// MARK: - PREVIEW

struct TopUpView_Previews: PreviewProvider {
    static var previews: some View {
        TopUpView()
    }
}
