//
//  DashboardTopUpView.swift
//  Test
//
//  Created by Hera on 04/12/20.
//

import SwiftUI

struct DashboardTopUpView: View {
    
    var body: some View {
        NavigationView {
            GeometryReader { geo in
                VStack(alignment: .trailing, spacing: 15) {
                    NavigationLink(destination: TopUpGridView()) {
                        Text("Lihat Semua")
                            .padding(.trailing, 15)
                            .foregroundColor(.accentColor)
                    }
                    TopUpView()
                }
                .padding(.top, 10)
                .padding(.bottom, 10)
                .background(Color.white)
                .cornerRadius(10)
                .shadow(radius: 5)
                .navigationBarHidden(true)
            }
            .padding(.horizontal, 10)
        }
    }
}

struct DashboardTopUpView_Previews: PreviewProvider {
    static var previews: some View {
        DashboardTopUpView()
    }
}
