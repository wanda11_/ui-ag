//
//  HighlightView.swift
//  Test
//
//  Created by Hera on 27/11/20.
//

import SwiftUI

struct DashboardHighlightView: View {
    
    // MARK: - PROPERTIES
    
    // MARK: - BODY
    
    var body: some View {
        VStack(alignment: .center) {
            Text("This Week's Highlight")
                .bold()
                .font(.title2)
            
            NavigationView {
                GeometryReader { geo in
                    NavigationLink(destination: HighlightView()) {
                        ZStack(alignment: .bottomLeading) {
                            Image("2")
                                .resizable()
                                .clipped()
                            VStack {
                                Text("Byhers Now Available")
                                    .padding()
                                    .foregroundColor(.white)
                            }
                            .edgesIgnoringSafeArea(.all)
                            .frame(width: geo.size.width, height: 50, alignment: .bottomLeading)
                            .background(BlurView(style: .systemUltraThinMaterial))
                            .fixedSize()
                        }
                    }
                }
                .navigationBarHidden(true)
            }
        }
    }
}

// MARK: - PREVIEW

struct DashboardHighlightView_Previews: PreviewProvider {
    static var previews: some View {
        DashboardHighlightView()
    }
}
