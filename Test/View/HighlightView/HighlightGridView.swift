//
//  HighlightDetailView.swift
//  Test
//
//  Created by Hera on 03/12/20.
//

import SwiftUI

struct Box {
    var id: Int
    let title, imageURL: String
}

struct HighlightGridView: View {
    
    let boxes: [Box] = [
        Box(id: 0, title: "Byhers Now Available", imageURL: "0"),
        Box(id: 1, title: "Summer Sale", imageURL: "1"),
        Box(id: 2, title: "Sabichi Flash Sale Selected Item", imageURL: "2"),
        Box(id: 3, title: "Summer Sale", imageURL: "3")
    ]
    
    let columns = [
        GridItem(.flexible()),
        GridItem(.flexible())
    ]
    
    var body: some View {
        NavigationView {
            ScrollView(.vertical, showsIndicators: false) {
                LazyVGrid(columns: columns, spacing: 20) {
                    ForEach(boxes, id: \.id) { box in
                        BoxView(box: box)
                            .fixedSize()
                    }
                }
            }
            .navigationBarHidden(true)
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct BoxView: View {
    
    let box: Box
    
    var body: some View {
        VStack {
            NavigationLink(destination: DetailView()) {
                Image(box.imageURL)
                    .resizable()
                    .clipped()
                    .frame(width: 175, height: 175)
                    .cornerRadius(10)
            }
            Text(box.title)
                .font(.subheadline)
                .fontWeight(.bold)
        }
        .navigationBarTitle("HIGHLIGHT")
    }
}


struct HighlightGridView_Previews: PreviewProvider {
    static var previews: some View {
        HighlightGridView()
    }
}
