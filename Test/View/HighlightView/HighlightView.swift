//
//  Highlight.swift
//  Test
//
//  Created by Hera on 02/12/20.
//

import SwiftUI

struct HighlightView: View {
    
    @State var search: String = ""
    
    @State private var isEditing = true
    
    var body: some View {
        GeometryReader { geo in
            VStack {
                HStack {
                    TextField("Search", text: $search)
                        .padding(7)
                        .onTapGesture(perform: {
                            isEditing = true
                        })
                        .animation(.default)
                    Image(systemName: "magnifyingglass")
                        .foregroundColor(Color.gray)
                        .padding(.trailing, 10)
                }
                .background(Color.white)
                .cornerRadius(10)
                .shadow(radius: 5)
                .padding()
                ScrollView {
                    HighlightGridView()
                        .frame(height: 700)
                }
            }
        }
        .navigationBarTitle("HIGHLIGHT", displayMode: .inline)
    }
}



struct HighlightView_Previews: PreviewProvider {
    static var previews: some View {
        HighlightView()
    }
}
