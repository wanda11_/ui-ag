//
//  AGStoriesView.swift
//  Test
//
//  Created by Hera on 03/12/20.
//

import SwiftUI

struct AGStoriesView: View {
    
    @State var search: String = ""
    
    @State private var isEditing = true
    
    var body: some View {
        GeometryReader { geo in
            VStack {
                HStack {
                    TextField("Search", text: $search)
                        .padding(7)
                        .onTapGesture(perform: {
                            isEditing = true
                        })
                        .animation(.default)
                    Image(systemName: "magnifyingglass")
                        .foregroundColor(Color.gray)
                        .padding(.trailing, 10)
                }
                .background(Color.white)
                .cornerRadius(10)
                .shadow(radius: 5)
                .padding()
                .navigationBarTitle("AG Stories", displayMode: .inline)
                ScrollView {
                    AGStoriesGridView()
                        .frame(height: 700)
                }
            }
        }
    }
}

struct AGStoriesView_Previews: PreviewProvider {
    static var previews: some View {
        AGStoriesView()
    }
}
