//
//  AGStoriesGridView.swift
//  Test
//
//  Created by Hera on 03/12/20.
//

import SwiftUI

struct stories {
    var id: Int
    let title, subheadline , imageURL: String
}

struct AGStoriesGridView: View {
    
    let stori: [stories] = [
        stories(id: 0, title: "Byhers Now Available", subheadline: "authenticguards.com", imageURL: "0"),
        stories(id: 1, title: "Summer Sale", subheadline: "authenticguards.com", imageURL: "1"),
        stories(id: 2, title: "Sabichi Flash Sale Selected Item", subheadline: "authenticguards.com", imageURL: "2"),
        stories(id: 3, title: "Summer Sale", subheadline: "authenticguards.com", imageURL: "3")
    ]
    
    let columns = [
        GridItem(.flexible()),
        GridItem(.flexible())
    ]
    
    var body: some View {
        NavigationView {
            ScrollView(.vertical, showsIndicators: false) {
                LazyVGrid(columns: columns, spacing: 20) {
                    ForEach(stori, id: \.id) { stor in
                        AGView(str: stor)
                            .fixedSize()
                    }
                }
            }
            .navigationBarHidden(true)
        }
        .navigationBarTitle("AG Stories")
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct AGView: View {
    
    let str: stories
    
    var body: some View {
        NavigationLink(destination: DetailllView()) {
            VStack {
                Image(str.imageURL)
                    .resizable()
                    .clipped()
                    .frame(width: 175, height: 175)
                    .cornerRadius(10)
                Text(str.title)
                    .font(.subheadline)
                    .fontWeight(.bold)
                Text(str.subheadline)
                    .font(.subheadline)
                    .font(.footnote)
                    .foregroundColor(.gray)
            }
        }
        .foregroundColor(.black)
    }
}

struct AGStoriesGridView_Previews: PreviewProvider {
    static var previews: some View {
        AGStoriesGridView()
    }
}
