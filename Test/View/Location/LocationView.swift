//
//  LocationView.swift
//  Test
//
//  Created by Hera on 04/12/20.
//

import SwiftUI

struct LocationView: View {
    var body: some View {
        ZStack(alignment: Alignment.top) {
                    MapView()
                    SlideOverCard {
                        GeometryReader { geo in
                            VStack {
                                Image("0")
                                    .resizable()
                                    .scaledToFit()
                                    .scaledToFit()
                                Text("Nama Toko")
                                Text("Alamat")
                                Text("Jarak")
                                Button(action: {
                                    //
                                }, label: {
                                    Text("Call Store")
                                        .foregroundColor(.white)
                                        .font(.title2)
                                        .bold()
                                })
                                .frame(width: geo.size.width / 1.2)
                                .padding()
                                .background(Color.accentColor)
                                .cornerRadius(25)
                                .shadow(radius: 5)
                                Button(action: {
                                    //
                                }, label: {
                                    Text("Get Direction")
                                        .foregroundColor(.white)
                                        .font(.title2)
                                        .bold()
                                })
                                .frame(width: geo.size.width / 1.2)
                                .padding()
                                .background(Color.accentColor)
                                .cornerRadius(25)
                                .shadow(radius: 5)
                                
                                Spacer()
                            }
                        }
                    }
                }
                .edgesIgnoringSafeArea(.vertical)
    }
}

struct LocationView_Previews: PreviewProvider {
    static var previews: some View {
        LocationView()
    }
}
