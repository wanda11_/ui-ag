//
//  Handle.swift
//  Test
//
//  Created by Hera on 04/12/20.
//

import SwiftUI

struct Handle: View {
    
    private let handleThicness = CGFloat(5.0)
    
    var body: some View {
        RoundedRectangle(cornerRadius: handleThicness / 2)
            .frame(width: 40, height: handleThicness)
            .foregroundColor(Color.secondary)
            .padding(5)
    }
}

struct Handle_Previews: PreviewProvider {
    static var previews: some View {
        Handle()
    }
}
