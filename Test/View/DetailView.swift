//
//  DetailView.swift
//  Test
//
//  Created by Hera on 04/12/20.
//

import SwiftUI

struct DetailView: View {
    
    @State private var showAlert = false
    
    var alert: Alert {
        Alert(title: Text("How To Use"), message: Text("Bagaimana cara menggunakan aplikasi Authentic Guards?"), dismissButton: .default(Text("OK")))
    }
    
    var body: some View {
        GeometryReader { geo in
            ScrollView(.vertical, showsIndicators: false) {
                VStack(alignment: .center) {
                    Image("0")
                        .resizable()
                        .clipped()
                        .frame(height: 200)
                    VStack(alignment: .leading) {
                        Text("Sabichi flash sale selected item")
                            .bold()
                            .font(.title2)
                        Text("500.00 Point")
                            .padding(.bottom)
                        HStack {
                            Text("Available Until")
                            Spacer()
                            Text("2017-09-19 23:53:09")
                        }
                        .padding(.bottom)
                        HStack {
                            Text("How To Use")
                                .bold()
                            Spacer()
                            Button(action: {
                                self.showAlert.toggle()
                            }, label: {
                                Image(systemName: "questionmark.diamond.fill")
                                    .resizable()
                                    .frame(width: 25, height: 25)
                                    .foregroundColor(.accentColor)
                            })
                            .alert(isPresented: $showAlert, content: { self.alert })
                        }
                        Text("Description")
                            .bold()
                        Text("Term and Reference")
                            .bold()
                    }
                    .padding()
                    
                    Spacer()
                    
                    Button(action: {
                        //
                    }, label: {
                        Text("REDEEM")
                            .foregroundColor(.white)
                            .bold()
                            .font(.title2)
                    })
                    .frame(width: geo.size.width / 1.2)
                    .padding()
                    .background(Color.accentColor)
                    .cornerRadius(25)
                    .shadow(radius: 5)
                    
                    Spacer()
                }
            }
        }
    }
}

struct DetailView_Previews: PreviewProvider {
    static var previews: some View {
        DetailView()
    }
}
