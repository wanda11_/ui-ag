//
//  TransferBankBNIView.swift
//  Test
//
//  Created by Hera on 17/12/20.
//

import SwiftUI

struct TransferBankBNIView: View {
    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            VStack(spacing: 13) {
                VStack {
                    HStack {
                        Text("SEGERA LAKUKAN PEMBAYARAN DALAM WAKTU")
                            .bold()
                            .font(.system(size: 13))
                    }
                    HStack {
                        Text("05 Jam : 51 Menit : 23 Detik")
                            .bold()
                            .font(.title2)
                    }
                    HStack {
                        Text("(Before Rabu, 16 Desember 2020, 07:40 WIB)")
                            .font(.subheadline)
                    }
                }
                .frame(width: 325, height: 75)
                .padding()
                .foregroundColor(.white)
                .background(Color.accentColor)
                
                Group {
                    HStack {
                        Spacer()
                        Text("Transfer to Bank Account Number")
                        Spacer()
                    }
                    HStack {
                        Spacer()
                        Image("bni")
                            .resizable()
                            .scaledToFit()
                            .frame(width: 100, height: 75)
                        Spacer()
                        Text("700 004 1139")
                        Image("sheet")
                            .resizable()
                            .scaledToFit()
                            .frame(width: 25, height: 25)
                        Spacer()
                    }
                }
                
                Divider().background(Color.accentColor)
                Group {
                    Text("Jumlah yang harus dibayar")
                    Text("Rp. 10,000")
                }
                .padding(.leading, 20)
                Divider().background(Color.accentColor)
                
                Text("Panduan Pembayaran")
                    .bold()
                    .padding(.leading, 20)
                
                GroupBox {
                    DisclosureGroup("Petunjuk Transfer ATM") {
                        VStack(alignment: .leading) {
                            HStack {
                                Text("1. Pilih Bahasa Indonesia.")
                            }
                            HStack {
                                Text("2. Masukkan 6 Digit PIN -> Benar")
                            }
                            HStack {
                                Text("3. Pilih Menu Lain")
                            }
                            HStack {
                                Text("4. Pilih dari Rekening Tabungan")
                            }
                            HStack {
                                Text("5. Pilih ke Rekening BNI")
                            }
                            HStack {
                                Text("6. Maukkan nomor Rekening AGT 7000041139 -> Benar")
                            }
                            HStack {
                                Text("7. Masukkan nominal Top-Up yang sudah termasuk nomor unik -> Benar")
                            }
                            HStack {
                                Text("8. Pilih Benar lagi")
                            }
                            HStack {
                                Text("9. Konfirmasi jika benar pilih (YA)")
                            }
                            HStack {
                                Text("10. Transaksi Selesai, pilih Tidak untuk Selesai")
                            }
                        }
                    }
                }
                Group {
                    Button(action: {
                        //
                    }, label: {
                        HStack {
                            Spacer()
                            Image(systemName: "arrow.up.doc")
                                .resizable()
                                .scaledToFit()
                                .frame(width: 25, height: 25)
                            Text("UPLOAD PAYMENT PROOF")
                                .bold()
                                .font(.headline)
                            Spacer()
                        }
                        .foregroundColor(.white)
                    })
                    .padding(.vertical, 10)
                    .padding(.horizontal, 10)
                    .background(Color.accentColor)
                    .cornerRadius(10)
                    
                    Button(action: {
                        //
                    }, label: {
                        HStack {
                            Spacer()
                            Image(systemName: "xmark.circle")
                                .resizable()
                                .scaledToFit()
                                .frame(width: 25, height: 25)
                            Text("CANCEL PURCHASE")
                                .bold()
                                .font(.headline)
                            Spacer()
                        }
                        .foregroundColor(.white)
                    })
                    .padding(.vertical, 10)
                    .padding(.horizontal, 10)
                    .background(Color.accentColor)
                    .cornerRadius(10)
                }
            }
            .padding(.top, 10)
            .padding(.bottom, 10)
        }
        .padding(.horizontal, 10)
        .navigationViewStyle(StackNavigationViewStyle())
        .navigationBarTitle("PEMBAYARAN", displayMode: .inline)
    }
}

struct TransferBankBNIView_Previews: PreviewProvider {
    static var previews: some View {
        TransferBankBNIView()
    }
}
