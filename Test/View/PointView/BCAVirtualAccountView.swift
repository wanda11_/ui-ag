//
//  BCAVirtualAccountView.swift
//  Test
//
//  Created by Hera on 17/12/20.
//

import SwiftUI

struct BCAVirtualAccountView: View {
    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            VStack(spacing: 13) {
                VStack {
                    HStack {
                        Text("SEGERA LAKUKAN PEMBAYARAN DALAM WAKTU")
                            .bold()
                            .font(.system(size: 13))
                    }
                    HStack {
                        Text("05 Jam : 51 Menit : 23 Detik")
                            .bold()
                            .font(.title2)
                    }
                    HStack {
                        Text("(Before Rabu, 16 Desember 2020, 07:40 WIB)")
                            .font(.subheadline)
                    }
                }
                .frame(width: 325, height: 75)
                .padding()
                .foregroundColor(.white)
                .background(Color.accentColor)
                
                Group {
                    HStack {
                        Spacer()
                        Text("Transfer to Bank Account Number")
                        Spacer()
                    }
                    HStack {
                        Spacer()
                        Image("bca")
                            .resizable()
                            .scaledToFit()
                            .frame(width: 100, height: 75)
                        Spacer()
                        Text("1268885320233328")
                        Image("sheet")
                            .resizable()
                            .scaledToFit()
                            .frame(width: 25, height: 25)
                        Spacer()
                    }
                    
                    Text("Checked in 10 minutes after payment succeed")
                    Text("Pay your order Virtual Account before making a new order on Virtual Account to keep the number the same. Hanya dari bank BCA.")
                }
                
                Divider().background(Color.accentColor)
                Group {
                    Text("Jumlah yang harus dibayar")
                    Text("Rp. 10,000")
                }
                .padding(.leading, 20)
                Divider().background(Color.accentColor)
                
                Text("Panduan Pembayaran")
                    .bold()
                    .padding(.leading, 20)
                
                GroupBox {
                    DisclosureGroup("Petunjuk Transfer ATM") {
                        VStack(alignment: .leading) {
                            HStack {
                                Text("1. Masukkan kartu ATM & PIN BCA Anda.")
                            }
                            HStack {
                                Text("2. Pilih Menu Transaksi Lainnya.")
                            }
                            HStack {
                                Text("3. Pilih menu Transfer")
                            }
                            HStack {
                                Text("4. Masukkan 39358 + nomor ponsel Anda")
                            }
                            HStack {
                                Text("5. 39358 + 08xx-xxxx-xxxx")
                            }
                            HStack {
                                Text("6 Masukkan nominal Top-Up")
                            }
                            HStack {
                                Text("7. Ikuti instruksi untuk menyelesaikan transaksi")
                            }
                            
                        }
                    }
                    Divider().background(Color.accentColor)
                    DisclosureGroup("Petunjuk Transfer m-Banking / m-BCA") {
                        ScrollView(.vertical, showsIndicators: false) {
                            VStack(alignment: .leading) {
                                Group {
                                    HStack {
                                        Text("1. Login ke akun m-BCA Anda.")
                                    }
                                    HStack {
                                        Text("2. Pilih menu m-Transfer.")
                                    }
                                    HStack {
                                        Text("3. Pilih BCA Virtual Account.")
                                    }
                                    HStack {
                                        Text("4. Masukkan 39358 + nomor ponsel Anda.")
                                    }
                                    HStack {
                                        Text("5. 39358 + 08xx-xxxx-xxxx")
                                    }
                                }
                                HStack {
                                    Text("6. Masukkan nominal Top-Up.")
                                }
                                HStack {
                                    Text("7. Ikuti instruksi untuk menyelesaikan transaksi.")
                                    
                                }
                            }
                        }
                    }
                }
                Group {
                    Button(action: {
                        //
                    }, label: {
                        HStack {
                            Spacer()
                            Image(systemName: "arrow.up.doc")
                                .resizable()
                                .scaledToFit()
                                .frame(width: 25, height: 25)
                            Text("UPLOAD PAYMENT PROOF")
                                .bold()
                                .font(.headline)
                            Spacer()
                        }
                        .foregroundColor(.white)
                    })
                    .padding(.vertical, 10)
                    .padding(.horizontal, 10)
                    .background(Color.accentColor)
                    .cornerRadius(10)
                    
                    Button(action: {
                        //
                    }, label: {
                        HStack {
                            Spacer()
                            Image(systemName: "xmark.circle")
                                .resizable()
                                .scaledToFit()
                                .frame(width: 25, height: 25)
                            Text("CANCEL PURCHASE")
                                .bold()
                                .font(.headline)
                            Spacer()
                        }
                        .foregroundColor(.white)
                    })
                    .padding(.vertical, 10)
                    .padding(.horizontal, 10)
                    .background(Color.accentColor)
                    .cornerRadius(10)
                }
            }
            .padding(.top, 10)
            .padding(.bottom, 10)
        }
        .padding(.horizontal, 10)
        .navigationViewStyle(StackNavigationViewStyle())
        .navigationBarTitle("PEMBAYARAN", displayMode: .inline)
    }
}

struct BCAVirtualAccountView_Previews: PreviewProvider {
    static var previews: some View {
        BCAVirtualAccountView()
    }
}
