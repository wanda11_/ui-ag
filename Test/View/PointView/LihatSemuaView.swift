//
//  LihatSemuaView.swift
//  Test
//
//  Created by Hera on 16/12/20.
//

import SwiftUI

struct LihatSemuaView: View {
    
//    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        NavigationView {
            ScrollView(.vertical, showsIndicators: false) {
                VStack {
                    BankView()
                }
                .navigationBarTitle("METODE PEMBAYARAN", displayMode: .inline)
            }
        }
        .cornerRadius(15)
        .shadow(radius: 5)
    }
}

struct LihatSemuaView_Previews: PreviewProvider {
    static var previews: some View {
        LihatSemuaView()
    }
}
