//
//  DetailWalletView.swift
//  Test
//
//  Created by Hera on 15/12/20.
//

import SwiftUI

struct DetailWalletView: View {
    
    @State var trans: String = "transaksi"
    
    var body: some View {
        VStack {
            Picker("Transaksi", selection: $trans) {
                Text("TRANSAKSI").tag("transaksi")
                Text("RIWAYAT").tag("history")
            }
            .pickerStyle(SegmentedPickerStyle())
            .padding(.top, 10)
            if trans == "transaksi" {
                ShowView()
            } else {
                RiwayatView()
            }
        }
        .navigationViewStyle(StackNavigationViewStyle())
        .navigationBarTitle("Wallet", displayMode: .inline)
    }
}

struct DetailWalletView_Previews: PreviewProvider {
    static var previews: some View {
        DetailWalletView()
    }
}
