//
//  ShowView.swift
//  Test
//
//  Created by Hera on 14/12/20.
//

import SwiftUI

struct ShowView: View {
    
    @State private var isShowingLihat: Bool = false
    
    var body: some View {
        NavigationView {
            GeometryReader { geo in
                VStack(spacing: 20) {
                    TransaksiView()
                        .frame(height: 235)
                    HStack {
                        Text("Metode Pembayaran")
                        Spacer()
                        Button(action: {
                            isShowingLihat = true
                        }, label: {
                            Text("Lihat Semua")
                                .foregroundColor(.accentColor)
                        })
                        .sheet(isPresented: $isShowingLihat) {
                            LihatSemuaView()
                        }
                    }
                    HStack {
                        Image("bca")
                            .resizable()
                            .scaledToFit()
                            .frame(width: 100, height: 50)
                        Text("BCA Virtual Account")
                            .foregroundColor(.gray)
                        Spacer()
                    }
                    HStack {
                        Text("Biaya Admin :")
                        Text("Rp 2,700")
                            .foregroundColor(.accentColor)
                        Spacer()
                    }
                    
                    VStack {
                        Button(action: {
                            //
                        }, label: {
                            NavigationLink(destination: DetailPembayaranPulsaView()) {
                                Text("LANJUTKAN PEMBAYARAN")
                                    .foregroundColor(.white)
                                    .bold()
                                    .font(.title2)
                            }
                        })
                        .frame(width: geo.size.width / 1.2)
                        .padding()
                        .background(Color.accentColor)
                        .cornerRadius(25)
                        .shadow(radius: 5)
                    }
                    .padding(.top, 30)
                    
                    Spacer()
                }
                .padding(.horizontal, 10)
            }
            .navigationBarHidden(true)
        }
    }
}

struct ShowView_Previews: PreviewProvider {
    static var previews: some View {
        ShowView()
    }
}
