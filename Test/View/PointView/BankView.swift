//
//  BankView.swift
//  Test
//
//  Created by Hera on 16/12/20.
//

import SwiftUI

struct BankView: View {
    var body: some View {
        VStack(alignment: .leading, spacing: 10) {
            Group {
                Text("Transfer Virtual Account")
                    .bold()
                    .font(.title3)
                HStack {
                    Image("bca")
                        .resizable()
                        .scaledToFit()
                        .frame(width: 100, height: 50)
                    Spacer()
                    Text("BCA Virtual Account")
                }
                Divider().background(Color.black)
                HStack {
                    Image("bni")
                        .resizable()
                        .scaledToFit()
                        .frame(width: 100, height: 50)
                    Spacer()
                    Text("BNI Virtual Account")
                }
                Divider().background(Color.black)
            }
            
            Group {
                Text("Transfer Bank")
                    .bold()
                    .font(.title3)
                HStack {
                    Image("bca")
                        .resizable()
                        .scaledToFit()
                        .frame(width: 100, height: 50)
                    Spacer()
                    Text("Transfer Bank BCA")
                }
                Divider().background(Color.black)
                HStack {
                    Image("bni")
                        .resizable()
                        .scaledToFit()
                        .frame(width: 100, height: 50)
                    Spacer()
                    Text("Transfer Bank BNI")
                }
                Divider().background(Color.black)
                HStack {
                    Image("bmandiri")
                        .resizable()
                        .scaledToFit()
                        .frame(width: 100, height: 50)
                    Spacer()
                    Text("Transfer Bank Mandiri")
                }
                Divider().background(Color.black)
                HStack {
                    Image("bri")
                        .resizable()
                        .scaledToFit()
                        .frame(width: 100, height: 50)
                    Spacer()
                    Text("Transfer Bank BRI")
                }
                Divider().background(Color.black)
            }
            Group {
                Text("Lainnya")
                HStack {
                    Image("logo_cashlez")
                        .resizable()
                        .scaledToFit()
                        .frame(width: 140, height: 30)
                        .background(Color.black)
                        .padding()
                    Spacer()
                    Text("Cashlez")
                }
                Divider().background(Color.black)
            }
        }
        .padding()
    }
}

struct BankView_Previews: PreviewProvider {
    static var previews: some View {
        BankView()
    }
}
