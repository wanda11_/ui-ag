//
//  TransaksiView.swift
//  Test
//
//  Created by Hera on 10/12/20.
//

import SwiftUI

struct trans {
    var id: Int
    let text: String
}

struct TransaksiView: View {
    
    let transaksi: [trans] = [
        trans(id: 0, text: "10,000"),
        trans(id: 1, text: "20,000"),
        trans(id: 2, text: "50,000"),
        trans(id: 3, text: "100,000"),
        trans(id: 4, text: "250,000"),
        trans(id: 5, text: "500,000"),
        trans(id: 6, text: "1,000,000"),
        trans(id: 7, text: "2,000,000")
    ]
    
    let columns = [
        GridItem(.flexible()),
        GridItem(.flexible())
    ]
    
    var body: some View {
        GeometryReader { geo in
            LazyVGrid(columns: columns, spacing: 20) {
                ForEach(transaksi, id: \.id) { tp in
                    tv(trv: tp)
                        .frame(width: geo.size.width, height: 40)
                }
            }
            .padding(.top, 10)
            .navigationBarTitle("Wallet", displayMode: .inline)
        }
    }
}

struct tv: View {
    
    var trv: trans
    
    var body: some View {
        Text(trv.text)
            .bold()
            .font(.title3)
            .frame(width: 125, height: 10)
            .padding()
            .background(Color.white)
            .cornerRadius(15)
            .shadow(radius: 5)
    }
}

struct TransaksiView_Previews: PreviewProvider {
    static var previews: some View {
        TransaksiView()
    }
}
