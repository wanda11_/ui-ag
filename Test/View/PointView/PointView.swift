//
//  PointView.swift
//  Test
//
//  Created by Hera on 27/11/20.
//

import SwiftUI

struct PointView: View {
    
    // MARK: - PROPERTIES
    
    // MARK: - BODY
    
    var body: some View {
        NavigationView {
            GeometryReader { geo in
                HStack {
                    HStack {
                        // MARK: - WALLET
                        Spacer()
                        
                        NavigationLink(destination: DetailWalletView()) {
                            Image("dashboard - wallet")
                                .resizable()
                                .scaledToFit()
                                .frame(width: 30, height: 30)
                            
                            VStack(alignment: .leading) {
                                Text("Rp. 0")
                                Text("Isi Saldo AGT")
                            }
                        }
                        
                        Spacer()
                        
                        Divider().background(Color.accentColor).frame(height: 40)
                        
                        // MARK: - COIN
                        Image("dashboard - coin")
                            .resizable()
                            .scaledToFit()
                            .frame(width: 30, height: 30)
                        
                        VStack(alignment: .leading) {
                            Text("Rp. 0")
                            Text("Kumpulkan Point AGT")
                        }
                        
                        Spacer()
                    } //: HSTACK
                    .font(.footnote)
                    .foregroundColor(.accentColor)
                } //: GROUPBOX
                .frame(width: geo.size.width)
                .padding(.vertical, 5)
                .background(Color.white)
                .cornerRadius(20)
                .shadow(radius: 5)
            }
            .padding(.top, 5)
            .padding(.horizontal, 10)
            .navigationBarHidden(true)
        }
    }
}

// MARK: - PREVIEW

struct PointView_Previews: PreviewProvider {
    static var previews: some View {
        PointView()
    }
}
