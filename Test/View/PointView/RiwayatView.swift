//
//  RiwayatView.swift
//  Test
//
//  Created by Hera on 14/12/20.
//

import SwiftUI

struct RiwayatView: View {
    var body: some View {
        GeometryReader { geo in
            HStack {
                Image("paid")
                    .resizable()
                    .scaledToFit()
                    .frame(width: 60, height: 60)
                VStack {
                    HStack {
                        Text("Paid")
                            .bold()
                        Spacer()
                        Text("2020-07-09 15:37:29")
                    }
                    HStack {
                        Text("Rp. 10960")
                            .bold()
                        Spacer()
                        Text("BCA")
                    }
                }
            }
            .padding()
            .frame(width: geo.size.width)
        }
    }
}

struct RiwayatView_Previews: PreviewProvider {
    static var previews: some View {
        RiwayatView()
    }
}
