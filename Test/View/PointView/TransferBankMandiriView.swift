//
//  TransferBankMandiriView.swift
//  Test
//
//  Created by Hera on 17/12/20.
//

import SwiftUI

struct TransferBankMandiriView: View {
    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            VStack(spacing: 13) {
                VStack {
                    HStack {
                        Text("SEGERA LAKUKAN PEMBAYARAN DALAM WAKTU")
                            .bold()
                            .font(.system(size: 13))
                    }
                    HStack {
                        Text("05 Jam : 51 Menit : 23 Detik")
                            .bold()
                            .font(.title2)
                    }
                    HStack {
                        Text("(Before Rabu, 16 Desember 2020, 07:40 WIB)")
                            .font(.subheadline)
                    }
                }
                .frame(width: 325, height: 75)
                .padding()
                .foregroundColor(.white)
                .background(Color.accentColor)
                
                Group {
                    HStack {
                        Spacer()
                        Text("Transfer to Bank Account Number")
                        Spacer()
                    }
                    HStack {
                        Spacer()
                        Image("bmandiri")
                            .resizable()
                            .scaledToFit()
                            .frame(width: 100, height: 75)
                        Spacer()
                        Text("130 000 009 4113")
                        Image("sheet")
                            .resizable()
                            .scaledToFit()
                            .frame(width: 25, height: 25)
                        Spacer()
                    }
                }
                
                Divider().background(Color.accentColor)
                Group {
                    Text("Jumlah yang harus dibayar")
                    Text("Rp. 10,000")
                }
                .padding(.leading, 20)
                Divider().background(Color.accentColor)
                
                Text("Panduan Pembayaran")
                    .bold()
                    .padding(.leading, 20)
                
                GroupBox {
                    DisclosureGroup("Petunjuk Transfer ATM") {
                        VStack(alignment: .leading) {
                            HStack {
                                Text("1. Masukan kartu ATM Mandiri dengan posisi yang benar")
                            }
                            HStack {
                                Text("2. Pilih Menu Bahasa Indonesia")
                            }
                            HStack {
                                Text("3. Masukan 6 digit PIN Anda dengan benar")
                            }
                            HStack {
                                Text("4. Pilih menu transaksi lainnya")
                            }
                            HStack {
                                Text("5. Pilih menu Transfer")
                            }
                            HStack {
                                Text("6. Pilih menu ke Rekening Mandiri")
                            }
                            HStack {
                                Text("7. Masukan nomor rekening AGT 130 000 009 4113 (pilih Benar)")
                            }
                            HStack {
                                Text("8. Masukkan nominal Top-Up uang sudah termasuk nomor unik (pilih Benar)")
                            }
                            HStack {
                                Text("9. Transaksi Anda telah selesai, pilih Keluar")
                            }
                            HStack {
                                Text("10. Atau tekan Cancel")
                            }
                        }
                    }
                    
                    Divider().background(Color.black)
                    
                    DisclosureGroup("Petunjuk Transfer Mandiri Online") {
                        VStack(alignment: .leading) {
                            HStack {
                                Text("1. Login ke akun mandiri Online Anda")
                            }
                            HStack {
                                Text("2. Pilih Transfer")
                            }
                            HStack {
                                Text("3. Pilih ke Rekening Mandiri")
                            }
                            HStack {
                                Text("4. Tentukan Rekening Sumber")
                            }
                            HStack {
                                Text("5. Isi Rekening Tujuan dengan Rekening AGT 130 000 009 4113")
                            }
                            HStack {
                                Text("6. Dan nominal Top-Up yang sudah termasuk nomor unik sebagai Jumlah Transfer")
                            }
                            HStack {
                                Text("7. Ikuti instruksi untuk menyelesaikan transaksi")
                            }
                        }
                    }
                }
                Group {
                    Button(action: {
                        //
                    }, label: {
                        HStack {
                            Spacer()
                            Image(systemName: "arrow.up.doc")
                                .resizable()
                                .scaledToFit()
                                .frame(width: 25, height: 25)
                            Text("UPLOAD PAYMENT PROOF")
                                .bold()
                                .font(.headline)
                            Spacer()
                        }
                        .foregroundColor(.white)
                    })
                    .padding(.vertical, 10)
                    .padding(.horizontal, 10)
                    .background(Color.accentColor)
                    .cornerRadius(10)
                    
                    Button(action: {
                        //
                    }, label: {
                        HStack {
                            Spacer()
                            Image(systemName: "xmark.circle")
                                .resizable()
                                .scaledToFit()
                                .frame(width: 25, height: 25)
                            Text("CANCEL PURCHASE")
                                .bold()
                                .font(.headline)
                            Spacer()
                        }
                        .foregroundColor(.white)
                    })
                    .padding(.vertical, 10)
                    .padding(.horizontal, 10)
                    .background(Color.accentColor)
                    .cornerRadius(10)
                }
            }
            .padding(.top, 10)
            .padding(.bottom, 10)
        }
        .padding(.horizontal, 10)
        .navigationViewStyle(StackNavigationViewStyle())
        .navigationBarTitle("PEMBAYARAN", displayMode: .inline)
    }
}

struct TransferBankMandiriView_Previews: PreviewProvider {
    static var previews: some View {
        TransferBankMandiriView()
    }
}
