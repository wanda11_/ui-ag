//
//  DetailPembayaranPulsaView.swift
//  Test
//
//  Created by Hera on 16/12/20.
//

import SwiftUI

struct DetailPembayaranPulsaView: View {
    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            VStack(spacing: 13) {
                VStack {
                    HStack {
                        Text("SEGERA LAKUKAN PEMBAYARAN DALAM WAKTU")
                            .bold()
                            .font(.system(size: 13))
                    }
                    HStack {
                        Text("05 Jam : 51 Menit : 23 Detik")
                            .bold()
                            .font(.title2)
                    }
                    HStack {
                        Text("(Before Rabu, 16 Desember 2020, 07:40 WIB)")
                            .font(.subheadline)
                    }
                }
                .frame(width: 325, height: 75)
                .padding()
                .foregroundColor(.white)
                .background(Color.accentColor)
                
                Group {
                    HStack {
                        Spacer()
                        Text("Transfer to Bank Account Number")
                        Spacer()
                    }
                    HStack {
                        Spacer()
                        Image("bca")
                            .resizable()
                            .scaledToFit()
                            .frame(width: 100, height: 75)
                        Spacer()
                        //                        Text("7751064113")
                        Text("1268885320233328")
                        Image("sheet")
                            .resizable()
                            .scaledToFit()
                            .frame(width: 25, height: 25)
                        Spacer()
                    }
                    
                    Text("Checked in 10 minutes after payment succeed")
                    Text("Pay your order Virtual Account before making a new order on Virtual Account to keep the number the same. Hanya dari bank BCA.")
                }
                
                Divider().background(Color.accentColor)
                Group {
                    Text("Jumlah yang harus dibayar")
                    Text("Rp. 10,000")
                }
                .padding(.leading, 20)
                Divider().background(Color.accentColor)
                
                Text("Panduan Pembayaran")
                    .bold()
                    .padding(.leading, 20)
                
                GroupBox {
                    DisclosureGroup("Petunjuk Transfer ATM") {
                        VStack(alignment: .leading) {
                            HStack {
                                Text("1. Masukkan kartu ATM & PIN BCA Anda.")
                            }
                            HStack {
                                Text("2. Pilih Menu Transaksi Lainnya.")
                            }
                            HStack {
                                //                                Text("3. Pilih menu Transfer & ke Rekening BCA")
                                Text("3. Pilih menu Transfer")
                            }
                            HStack {
                                //                                Text("4. Masukkan nomor rekening AGT 7751064113")
                                Text("4. Masukkan 39358 + nomor ponsel Anda")
                            }
                            HStack {
                                //                                Text("5. Masukkan Nominal Top-Up yang sudah termasuk nomor unik")
                                Text("5. 39358 + 08xx-xxxx-xxxx")
                            }
                            HStack {
                                //                                Text("6. Ikuti instruksi untuk menyelesaikan transaksi.")
                                Text("6 Masukkan nominal Top-Up")
                            }
                            HStack {
                                Text("7. Ikuti instruksi untuk menyelesaikan transaksi")
                            }
                            
                        }
                    }
                    Divider().background(Color.accentColor)
                    DisclosureGroup("Petunjuk Transfer m-Banking / m-BCA") {
                        ScrollView(.vertical, showsIndicators: false) {
                            VStack(alignment: .leading) {
                                Group {
                                    HStack {
                                        //                                        Text("1. Pilih menu ''m-BCA'' di ponsel Anda dan tekan OK / YES.")
                                        Text("1. Login ke akun m-BCA Anda.")
                                    }
                                    HStack {
                                        //                                        Text("2. Pilih menu ''m-Transfer'' dan tekan OK / YES.")
                                        Text("2. Pilih menu m-Transfer.")
                                    }
                                    HStack {
                                        //                                        Text("3. Pilih ''Antar Rek'' dan tekan OK / YES.")
                                        Text("3. Pilih BCA Virtual Account.")
                                    }
                                    HStack {
                                        //                                        Text("4. Pilih ''Mata Uang (Rp / USD / SGD)'' dan tekan OK / YES.")
                                        Text("4. Masukkan 39358 + nomor ponsel Anda.")
                                    }
                                    HStack {
                                        //                                        Text("5. Masukkan nominal top-up yang sudah termasuk nomor unik dan tekan OK / YES.")
                                        Text("5. 39358 + 08xx-xxxx-xxxx")
                                    }
                                }
                                HStack {
                                    //                                    Text("6. Masukkan nomor rekening AGT 7751064113 dan tekan OK /Y ES.")
                                    Text("6. Masukkan nominal Top-Up.")
                                }
                                HStack {
                                    //                                    Text("7. Masukkan PIN m-BCA Anda dan tekan OK / YES.")
                                    Text("7. Ikuti instruksi untuk menyelesaikan transaksi.")
                                    
                                }
                                //                                HStack {
                                ////                                    Text("8. Muncul keterangan berita yang dapat Anda isi / kosongkan.")
                                //                                }
                                //                                HStack {
                                ////                                    Text("9. Jika Anda memiliki lebih dari 1 (satu) rekening yang terhubung dengan m-BCA, pilih nomor rekening yang akan digunakan kemudian tekan OK / YES.")
                                //                                }
                                //                                HStack {
                                ////                                    Text("10. Pastikan data transaksi yang muncul di layar konfirmasi telah benar. Jika benar, tekan OK / YES dan lanjutkan ke No.11. Jika tidak, diamkan selama beberapa saat sampai message tersebut hilang dan otomatis transaksi tersebut akan dibatalkan.")
                                //                                }
                                //                                HStack {
                                ////                                    Text("11. Masukkan PIN m-BCA Anda dan tekan OK / YES.")
                                //                                }
                            }
                        }
                    }
                }
                Group {
                    Button(action: {
                        //
                    }, label: {
                        HStack {
                            Spacer()
                            Image(systemName: "arrow.up.doc")
                                .resizable()
                                .scaledToFit()
                                .frame(width: 25, height: 25)
                            Text("UPLOAD PAYMENT PROOF")
                                .bold()
                                .font(.headline)
                            Spacer()
                        }
                        .foregroundColor(.white)
                    })
                    .padding(.vertical, 10)
                    .padding(.horizontal, 10)
                    .background(Color.accentColor)
                    .cornerRadius(10)
                    
                    Button(action: {
                        //
                    }, label: {
                        HStack {
                            Spacer()
                            Image(systemName: "xmark.circle")
                                .resizable()
                                .scaledToFit()
                                .frame(width: 25, height: 25)
                            Text("CANCEL PURCHASE")
                                .bold()
                                .font(.headline)
                            Spacer()
                        }
                        .foregroundColor(.white)
                    })
                    .padding(.vertical, 10)
                    .padding(.horizontal, 10)
                    .background(Color.accentColor)
                    .cornerRadius(10)
                }
            }
            .padding(.top, 10)
            .padding(.bottom, 10)
        }
        .padding(.horizontal, 10)
        .navigationViewStyle(StackNavigationViewStyle())
        .navigationBarTitle("PEMBAYARAN", displayMode: .inline)
    }
}

struct DetailPembayaranPulsaView_Previews: PreviewProvider {
    static var previews: some View {
        DetailPembayaranPulsaView()
    }
}
