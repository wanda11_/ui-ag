//
//  FakeReportView.swift
//  Test
//
//  Created by Hera on 15/12/20.
//

import SwiftUI

struct FakeReportView: View {
    
    @State var namaproduk: String = ""
    @State var alamat: String = ""
    @State var kota: String = ""
    @State var isilaporan: String = ""
    
    var body: some View {
        GeometryReader { geo in
            VStack {
                ZStack {
                    ScrollView(.vertical, showsIndicators: false) {
                        VStack(spacing: 20) {
                            Text("FAKE REPORT FORM")
                                .bold()
                                .font(.title2)
                            TextField("Nama Produk", text: $namaproduk)
                                .padding()
                                .background(Color.white)
                                .cornerRadius(5)
                                .shadow(radius: 3)
                            TextField("Alamat", text: $alamat)
                                .padding()
                                .background(Color.white)
                                .cornerRadius(5)
                                .shadow(radius: 3)
                            TextField("Kota", text: $kota)
                                .padding()
                                .background(Color.white)
                                .cornerRadius(5)
                                .shadow(radius: 3)
                            TextField("Isi Laporan", text: $isilaporan)
                                .padding()
                                .background(Color.white)
                                .cornerRadius(5)
                                .shadow(radius: 3)
                            Button(action: {
                                //
                            }, label: {
                                Text("SEND REPORT")
                                    .foregroundColor(.white)
                            })
                            .frame(width: geo.size.width / 1.2)
                            .padding()
                            .background(Color.accentColor)
                            .cornerRadius(25)
                            .shadow(radius: 5)
                            .buttonStyle(PlainButtonStyle())
                        }
                        .padding()
                        .background(Color.white)
                        .shadow(radius: 5)
                    }
                }
                
                Spacer()
                
                Button(action: {
                    //
                }, label: {
                    Text("CANCEL REPORT")
                        .foregroundColor(.white)
                })
                .frame(width: geo.size.width / 1.2)
                .padding()
                .background(Color.accentColor)
                .cornerRadius(25)
                .shadow(radius: 5)
                .buttonStyle(PlainButtonStyle())
                .padding(.bottom, 10)
            }
            .navigationBarHidden(true)
            .padding(.horizontal, 10)
            .frame(width: geo.size.width)
        }
    }
}

struct FakeReportView_Previews: PreviewProvider {
    static var previews: some View {
        FakeReportView()
    }
}
