//
//  FactView.swift
//  Test
//
//  Created by Hera on 14/12/20.
//

import SwiftUI

struct FactView: View {
    var body: some View {
        GeometryReader { geo in
            VStack(spacing: 20) {
                Spacer()
                Text("Unggah bukti pembayaran")
                Image("seo")
                    .resizable()
                    .clipped()
                    .frame(width: 200, height: 200)
                Button(action: {
                    //
                }, label: {
                    Text("PILIH FOTO")
                        .foregroundColor(.white)
                        .font(.title2)
                        .bold()
                })
                .frame(width: geo.size.width / 2)
                .padding()
                .background(Color.accentColor)
                .cornerRadius(25)
                .shadow(radius: 5)
                .buttonStyle(PlainButtonStyle())
                Text("Ukuran file yang diunggah maks. 2.5mb")
                    .foregroundColor(.gray)
                
                Spacer()
                
                Button(action: {
                    //
                }, label: {
                    Text("KIRIM BUKTI")
                        .foregroundColor(.white)
                        .font(.title2)
                        .bold()
                })
                .frame(width: geo.size.width / 1.2)
                .padding()
                .background(Color.accentColor)
                .cornerRadius(25)
                .shadow(radius: 5)
                .buttonStyle(PlainButtonStyle())
                .padding(.bottom, 10)
            }
            .navigationBarHidden(true)
            .frame(width: geo.size.width)
        }
    }
}

struct FactView_Previews: PreviewProvider {
    static var previews: some View {
        FactView()
    }
}
