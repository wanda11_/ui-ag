//
//  ClaimProductView.swift
//  Test
//
//  Created by Hera on 14/12/20.
//

import SwiftUI

struct ClaimProductView: View {
    var body: some View {
        NavigationView {
            GeometryReader { geo in
                ScrollView(.vertical, showsIndicators: false) {
                    VStack(spacing: 10) {
                        Group {
                            Spacer()
                            Image("2")
                                .resizable()
                                .clipped()
                                .frame(width: 175, height: 175)
                            Spacer()
                        }
                        Group {
                            VStack(alignment: .leading, spacing: 10) {
                                HStack {
                                    Text("Article")
                                        .bold()
                                    Text("ACNE TREATMENT SERUM")
                                        .padding(.leading, 70)
                                }
                                HStack {
                                    Text("Brand")
                                        .bold()
                                    Text("BYHERS COMPANY")
                                        .padding(.leading, 75)
                                }
                                HStack {
                                    Text("Size")
                                        .bold()
                                    Text("10 mL")
                                        .padding(.leading, 88)
                                }
                                HStack {
                                    Text("Color")
                                        .bold()
                                    Text("Yellow White")
                                        .padding(.leading, 78)
                                }
                                HStack {
                                    Text("Material")
                                        .bold()
                                    Text("Liquid")
                                        .padding(.leading, 57)
                                }
                                HStack {
                                    Text("Price")
                                        .bold()
                                    Text("Rp 135.000")
                                        .padding(.leading, 78)
                                }
                                HStack {
                                    Text("Distributor")
                                        .bold()
                                    Text("Distributor")
                                        .padding(.leading, 33)
                                }
                                HStack {
                                    Text("Expired Date")
                                        .bold()
                                    Text("Expired Date")
                                        .padding(.leading, 18)
                                }
                            }
                        }
                        .padding(.horizontal, 10)
                        Spacer()
                        Group {
                            Text("Are the details above in accordance with the item you bought?")
                                .font(.system(size: 12))
                            NavigationLink(destination: FakeReportView()) {
                                Text("If not report here")
                                    .foregroundColor(.accentColor)
                                    .font(.system(size: 12))
                            }
                            
                            Spacer()
                            
                            Button(action: {
                                //
                            }, label: {
                                NavigationLink(destination: FactView()) {
                                    Text("CLAIM YOUR REPORT")
                                        .foregroundColor(.white)
                                        .bold()
                                        .font(.title2)
                                }
                            })
                            .frame(width: geo.size.width / 1.16)
                            .padding()
                            .background(Color.accentColor)
                            .cornerRadius(10)
                            .shadow(radius: 5)
                            .buttonStyle(PlainButtonStyle())
                        }
                        .padding(.bottom, 10)
                    }
                    .frame(width: geo.size.width)
                }
                .navigationBarHidden(true)
            }
        }
    }
}

struct ClaimProductView_Previews: PreviewProvider {
    static var previews: some View {
        ClaimProductView()
    }
}
