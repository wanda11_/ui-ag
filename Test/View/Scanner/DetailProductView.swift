//
//  DetailProductView.swift
//  Test
//
//  Created by Hera on 14/12/20.
//

import SwiftUI

struct DetailProductView: View {
    var body: some View {
        NavigationView {
            GeometryReader { geo in
                VStack(alignment: .center) {
                    VStack {
                        HStack(alignment: .center) {
                            Spacer()
                            Image("Step 4 valid")
                                .resizable()
                                .frame(width: 125, height: 125, alignment: .center)
                            Spacer()
                        }
                        .padding()
                        HStack {
                            Spacer()
                            VStack {
                                Text("Congratulations !")
                                    .bold()
                                    .font(.title2)
                                Text("Your Product is Genuine !")
                                    .font(.subheadline)
                            }
                            Spacer()
                        }
                        .padding()
                    }
                    .background(Color.gray)
                    .edgesIgnoringSafeArea(.all)
                    
                    VStack(alignment: .leading, spacing: 10) {
                        Text("Your certified authentic product is :")
                        HStack {
                            Text("Code")
                            Text("AG-SWJTK")
                                .foregroundColor(.gray)
                                .padding(.leading, 35)
                        }
                        HStack {
                            Text("Brand")
                            Text("BYHERS")
                                .foregroundColor(.gray)
                                .padding(.leading, 28)
                        }
                        HStack {
                            Text("Company")
                            Text("PT. LASUARINDO")
                                .foregroundColor(.gray)
                        }
                        HStack {
                            Text("Address\n")
                            Text("Jl. Ahmadyani No. 669 Apartement Gateway")
                                .foregroundColor(.gray)
                                .padding(.leading, 10)
                        }
                        HStack {
                            Text("Phone")
                            Text("08226000411")
                                .foregroundColor(.gray)
                                .padding(.leading, 25)
                        }
                        HStack {
                            Text("Website")
                            Text("www.lasuarindo.co.id")
                                .foregroundColor(.gray)
                                .padding(.leading, 10)
                        }
                        HStack {
                            Text("Email")
                            Text("info@lasuarindo.co.id")
                                .foregroundColor(.gray)
                                .padding(.leading, 30)
                        }
                    }
                    .padding(.leading, 5)
                    
                    Spacer()
                    
                    Button(action: {
                        //
                    }, label: {
                        NavigationLink(destination: ClaimProductView()) {
                            Text("PRODUCT DETAIL")
                                .foregroundColor(.white)
                                .bold()
                                .font(.title2)
                        }
                    })
                    .frame(width: geo.size.width / 1.16)
                    .padding()
                    .background(Color.accentColor)
                    .cornerRadius(10)
                    .shadow(radius: 5)
                    .buttonStyle(PlainButtonStyle())
                }
                .padding(.bottom, 10)
                .frame(width: geo.size.width)
            }
            .navigationBarHidden(true)
        }
    }
}

struct DetailProductView_Previews: PreviewProvider {
    static var previews: some View {
        DetailProductView()
    }
}
