//
//  ScanView.swift
//  Test
//
//  Created by Hera on 04/12/20.
//

import SwiftUI

struct ScanView: View {
    var body: some View {
        NavigationView {
            GeometryReader { geo in
                VStack(alignment: .center, spacing: 20) {
                    Image("Step 2")
                        .resizable()
                        .scaledToFit()
                    HStack {
                        Text("Beberapa hal yang harus di perhatikan\nsebelum scan kde QR")
                        Spacer()
                    }
                    HStack {
                        Image(systemName: "wifi")
                            .resizable()
                            .clipped()
                            .frame(width: 40, height: 40)
                            .foregroundColor(.accentColor)
                        Text("Pastikan koneksi Anda dalam keadaan baik")
                        Spacer()
                    }
                    HStack {
                        Image("Step 4 valid")
                            .resizable()
                            .clipped()
                            .frame(width: 40, height: 40)
                        Text("Pastikan aplikasi Anda terupdate")
                        Spacer()
                    }
                    HStack {
                        Image("qr")
                            .resizable()
                            .clipped()
                            .frame(width: 40, height: 40)
                        Text("Pastikan marking QR Authenticguards terpasang pada produk Anda")
                        Spacer()
                    }
                    
                    Spacer()
                    
                    HStack(alignment: .center) {
                        Button(action: {
                            //
                        }, label: {
                            NavigationLink(destination: ScanQR()) {
                                Text("START")
                                    .foregroundColor(.white)
                                    .bold()
                                    .font(.title2)
                            }
                        })
                        .frame(width: geo.size.width / 1.35)
                        .padding()
                        .background(Color.accentColor)
                        .cornerRadius(25)
                        .shadow(radius: 5)
                    }
                    
                    Spacer()
                }
                .padding(.horizontal, 10)
                .frame(width: geo.size.width, height: geo.size.height)
            }
            .navigationBarHidden(true)
        }
        .navigationBarTitle("SCANNER", displayMode: .inline)
    }
}

struct ScanView_Previews: PreviewProvider {
    static var previews: some View {
        ScanView()
    }
}
