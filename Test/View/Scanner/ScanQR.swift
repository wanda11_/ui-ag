//
//  ScanQR.swift
//  Test
//
//  Created by Hera on 05/12/20.
//

import SwiftUI

struct ScanQR: View {
    var body: some View {
        GeometryReader { geo in
            Image("bg_content")
                .resizable()
                .scaledToFill()
                .frame(width: geo.size.width, height: geo.size.height)
            
        }
        .edgesIgnoringSafeArea(.all)
        .navigationBarHidden(true)
    }
}

struct ScanQR_Previews: PreviewProvider {
    static var previews: some View {
        ScanQR()
    }
}
