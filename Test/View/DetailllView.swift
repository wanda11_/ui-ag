//
//  DetailllView.swift
//  Test
//
//  Created by Hera on 08/12/20.
//

import SwiftUI

struct DetailllView: View {
    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            VStack(alignment: .leading, spacing: 5) {
                Text("Menristek Ingin Semua Pemalsu Ijazah Ditangkap Polisi")
                    .bold()
                    .font(.title)
                Text("Kompas.com")
                    .bold()
                    .font(.title3)
                Text("2017-12-07 01:47:43")
                Image("4")
                    .resizable()
                    .clipped()
                    .frame(width: 390, height: 250)
                    .cornerRadius(20)
                Text("Jakarta, Kompas.Com.\nMenteri Riset, Teknologi dan Pendidikan Tinggi, M.Nasir mengaku geram dengan masih maraknya pemalsuan ijazah yang dilakukan oleh oknum-oknum yang tidak bertanggung jawwab.\n\nHal itu ia ungkapkan menanggapi aksi kepolisian yang kembali membongkar praktik pemalsuan sertifikat dan ijazah palsu di Tambora, Jakarta Barat, Selasa (8/8/2017).\n\n\''Orang yang namanya mau berbuat jahat, semua cara dilakukan, makanya harus ditangkap habis'', ungkap Nasir di kantor Wakil Presiden RI, Jakarta, Jumat (11/8/2017).\n\nNasir mengakui, celah untuk melakukan pemalsuan ijazah tersebut selalu ada, meskipun berbagai upaya telah dilakukan pemerintah untuk mencegah pemalsuan. ''Makanya kamu harus bentengi itu, dengan sistem.\n\nMemang, ijazah sudah pakai Hologram, toh tetap ada yang memalsukan. Jangan sampai beredarnya ijazah palsu ini membuat masyarakat tek percaya lagi dengan ijazah,'' ungkap Nasir.\n\nSebelumnya, Polda Jakarta Barat menggrebek rumah di Siaga 1, Jalan Tubagus Angke, Tambora, Jakarta Barat yang menjadi markas pembuatan berbagai macam surat maupun dokumen palsu.\n\nDi tempat ini ditemukan berbagai perlengkapan pembuatan surat-surat palsu seperti surat sertifikasi guru, KTP, SKCK, ijazah dari SD hingga Perguruan Tinggi, surat tanah dan berbagai stempel dari berbagai macam instansi.\n\nSampai saat ini, polisi masih melakukan penelusuran lebih lanjut untuk mengetahui sejak kapan kegiatan pencetakan surat palsu ini dan kemana saja distribusinya.")
            }
        }
        .padding(.horizontal, 10)
    }
}

struct DetailllView_Previews: PreviewProvider {
    static var previews: some View {
        DetailllView()
    }
}
