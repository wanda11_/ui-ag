//
//  CoverImageView.swift
//  Test
//
//  Created by Hera on 26/11/20.
//

import SwiftUI
import SDWebImageSwiftUI

struct CoverImageView: View {
    
    // MARK: - PROPERTIES
    private var numberOfImages = 5
    private let timer = Timer.publish(every: 3, on: .main, in: .common).autoconnect()
    
    @State private var currentIndex = 0
    
    //    @ObservedObject var dataSlider = SliderFunction()
    
    // MARK: - BODY
    
    var body: some View {
        //            WebImage(url: URL(string: "https://admin.authenticguards.com/storage/image/slider-ea5d2f1c4608232e07d3aa3d998e5135.jpg"))
        //                .onSuccess { image, cacheType in
        //                    // Success
        //                }
        //                .resizable()
        //                .indicator(.activity)
        //                .animation(.easeInOut(duration: 0.5)) // Animation Duration
        //                .transition(.fade)
        //            if dataSlider.loading {
        //                Text("Tampilan Loading")
        //            } else if dataSlider.errorStatus != "" {
        //                Text("Tampilan error")
        //                Text("\(dataSlider.errorStatus)")
        //            } else if dataSlider.data.count > 0 {
        //                Text("Tampilan ketika ada data nya")
        //                ForEach(self.dataSlider.data, id: \.self){ item in // karena tidak memiliki id, jadi pake id: \.self
        //                    WebImage(url: URL(string: "https://admin.authenticguards.com/storage/\(item.image).jpg"))
        //                        .onSuccess { image, cacheType in
        //                            // Success
        //                        }
        //                        .resizable()
        //                        .indicator(.activity)
        //                        .animation(.easeInOut(duration: 0.5)) // Animation Duration
        //                        .transition(.fade)
        //                    Text(item.image)
        //                    Text(item.name)
        //                }
        //            } else {
        //                Text("Tampilan tidak ada data dari server")
        //            }
        GeometryReader { geo in
            TabView(selection: $currentIndex) {
                ForEach(0..<numberOfImages) { num in
                    Image("\(num)")
                        .resizable()
                        .clipped()
                        .cornerRadius(20)
                        .tag(num)
                } //: LOOP
            } //: TAB
            .tabViewStyle(PageTabViewStyle())
            .onReceive(timer, perform: { _ in
                withAnimation {
                    currentIndex = currentIndex < numberOfImages ? currentIndex + 1 : 0
                }
            })
            .frame(width: geo.size.width)
        }
        .padding(.horizontal, 10)
    }
}

// MARK: PREVIEW

struct CoverImageView_Previews: PreviewProvider {
    static var previews: some View {
        CoverImageView()
            .previewLayout(.fixed(width: 400, height: 275))
    }
}
