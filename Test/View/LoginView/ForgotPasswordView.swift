//
//  ForgotPasswordView.swift
//  Test
//
//  Created by Hera on 02/12/20.
//

import SwiftUI

struct ForgotPasswordView: View {
    
    @State var username: String = ""
    
    var body: some View {
        GeometryReader { geo in
            VStack(alignment: .center, spacing: 20) {
                Image("logo_ag")
                    .resizable()
                    .scaledToFit()
                    .frame(width: 120, height: 120, alignment: .center)
                VStack(alignment: .leading, spacing: 20) {
                    Text("We need your registered E-mail Address to sent you password reset instructions")
                    Text("E-MAIL ADDRESS")
                    TextField("Username", text: $username)
                        .padding()
                        .background(Color.white)
                        .cornerRadius(5)
                        .shadow(radius: 5)
                    Button(action: {
                        //
                    }, label: {
                        Text("SEND")
                            .bold()
                            .font(.title3)
                            .foregroundColor(.white)
                    })
                    .frame(width: geo.size.width / 1.09)
                    .padding()
                    .background(Color.accentColor)
                    .cornerRadius(25)
                    .shadow(radius: 5)
                }
            }
            .frame(width: geo.size.width)
            .navigationBarTitle("FORGOT PASSWORD", displayMode: .inline)
        }
        .padding()
    }
}

struct ForgotPasswordView_Previews: PreviewProvider {
    static var previews: some View {
        ForgotPasswordView()
    }
}
