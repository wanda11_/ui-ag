//
//  DaftarView.swift
//  Test
//
//  Created by Hera on 02/12/20.
//

import SwiftUI

struct DaftarView: View {
    
    @State var username: String = ""
    @State var password: String = ""
    
    @State private var isEditing = true
    
    var body: some View {
        GeometryReader { geo in
            ScrollView(.vertical, showsIndicators: false) {
                VStack(alignment: .center, spacing: 20) {
                    HStack {
                        Spacer()
                        Image("logo_ag")
                            .resizable()
                            .scaledToFit()
                            .frame(width: 120, height: 120, alignment: .center)
                        Spacer()
                    }
                    Text("Silahkan isi semua data untuk mendaftar !")
                    HStack {
                        Spacer()
                        VStack(alignment: .leading, spacing: 10) {
                            Text("NAMA LENGKAP")
                            TextField("", text: $username)
                                .padding()
                                .background(Color.white)
                                .cornerRadius(5)
                                .shadow(radius: 5)
                                .onTapGesture(perform: {
                                    isEditing = true
                                })
                                .animation(.default)
                            
                            Text("ALAMAT EMAIL")
                            TextField("", text: $username)
                                .padding()
                                .background(Color.white)
                                .cornerRadius(5)
                                .shadow(radius: 5)
                                .onTapGesture(perform: {
                                    isEditing = true
                                })
                                .animation(.default)
                            
                            Text("INSTAGRAM")
                            HStack{
                                Image(systemName: "at")
                                    .padding(.trailing, 17)
                                    .font(.title3)
                                TextField("(Optional)", text: $username)
                                    .padding()
                                    .font(.title3)
                                    .background(Color.white)
                                    .cornerRadius(5)
                                    .shadow(radius: 5)
                                    .onTapGesture(perform: {
                                        isEditing = true
                                    })
                                    .animation(.default)
                            }
                            
                            Group {
                                Text("NO. TELEPON")
                                HStack{
                                    Text("+62")
                                        .padding(.trailing, 5)
                                        .font(.title3)
                                    TextField("8xxxxx", text: $username)
                                        .padding()
                                        .font(.title3)
                                        .background(Color.white)
                                        .cornerRadius(5)
                                        .shadow(radius: 5)
                                        .onTapGesture(perform: {
                                            isEditing = true
                                        })
                                        .animation(.default)
                                }
                                
                                Text("PASSWORD")
                                SecureField("Password", text: $password)
                                    .padding()
                                    .font(.title3)
                                    .background(Color.white)
                                    .cornerRadius(5)
                                    .shadow(radius: 5)
                                    .onTapGesture(perform: {
                                        isEditing = true
                                    })
                                    .animation(.default)
                                
                                SecureField("Konfirmasi Password", text: $password)
                                    .padding()
                                    .font(.title3)
                                    .background(Color.white)
                                    .cornerRadius(5)
                                    .shadow(radius: 5)
                                    .onTapGesture(perform: {
                                        isEditing = true
                                    })
                                    .animation(.default)
                            }
                        }
                        
                        Spacer()
                    }
                    Button(action: {
                        //
                    }, label: {
                        Text("KIRIM")
                            .bold()
                            .font(.title3)
                            .foregroundColor(.white)
                    })
                    .frame(width: geo.size.width / 1.12)
                    .padding()
                    .background(Color.accentColor)
                    .cornerRadius(25)
                    .shadow(radius: 10)
                    .onTapGesture(perform: {
                        isEditing = true
                    })
                    .animation(.default)
                    
                    Spacer()
                    
                }            }
                .frame(width: geo.size.width)
                .navigationBarTitle("DAFTAR", displayMode: .inline)
        }
        .padding(.horizontal, 10)
        .padding(.top, 10)
    }
}


struct DaftarView_Previews: PreviewProvider {
    static var previews: some View {
        DaftarView()
    }
}
