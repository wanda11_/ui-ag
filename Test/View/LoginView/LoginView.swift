//
//  LoginView.swift
//  Test
//
//  Created by Hera on 30/11/20.
//

import SwiftUI

struct LoginView: View {
    
    @State var username: String = ""
    @State var password: String = ""
    
    var body: some View {
        NavigationView {
            GeometryReader { geo in
                ScrollView(.vertical, showsIndicators: false) {
                    VStack(spacing: 10) {
                        
                        Group {
                            Spacer()
                            
                            HStack(alignment: .center) {
                                Image("logo_ag")
                                    .resizable()
                                    .scaledToFit()
                                    .frame(width: 120, height: 120, alignment: .center)
                            }
                            
                            Spacer()
                            
                            Text("Silahkan masukkan email dan password Anda !")
                                .font(.subheadline)
                            
                            Spacer()
                        }
                        
                        VStack(alignment: .leading) {
                            Text("EMAIL")
                            TextField("Username", text: $username)
                                .padding()
                                .background(Color.white)
                                .cornerRadius(5)
                                .shadow(radius: 5)
                                .padding(.bottom, 20)
                            Text("PASSWORD")
                            SecureField("Password", text: $password)
                                .padding()
                                .background(Color.white)
                                .cornerRadius(5)
                                .shadow(radius: 5)
                                .padding(.bottom, 5)
                        }
                        .padding(.horizontal, 20)
                        
                        VStack(alignment: .trailing) {
                            NavigationLink(destination: ForgotPasswordView()) {
                                Text("Lupa Password ?")
                                    .foregroundColor(.accentColor)
                            }
                            
                            // MARK: - LOGIN
                            VStack {
                                Button(action: {
                                    //
                                }, label: {
                                    Text("LOG IN")
                                        .bold()
                                        .font(.title3)
                                        .foregroundColor(.white)
                                })
                                .frame(width: geo.size.width / 1.2)
                                .padding()
                                .background(Color.accentColor)
                                .cornerRadius(25)
                                .shadow(radius: 5)
                                
                                HStack {
                                    Text("Belum punya akun ?")
                                        .foregroundColor(.gray)
                                    NavigationLink(destination: DaftarView()) {
                                        Text("Klik Di Sini !")
                                            .foregroundColor(.accentColor)
                                    }
                                }
                            }
                        }
                        .padding(.horizontal, 10)
                        .padding(.vertical)
                    }
                }
                .frame(width: geo.size.width, height: geo.size.height)
            }
            .navigationBarHidden(true)
        }
        .navigationBarTitle("LOGIN", displayMode: .inline)
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
