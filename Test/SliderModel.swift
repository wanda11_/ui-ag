//
//  SliderModel.swift
//  Test
//
//  Created by Hera on 07/12/20.
//

import Foundation

struct SliderModel: Codable, Hashable { // klo ada id pake Identifiable
    var image: String
    var name: String
    
    init(image: String?, name: String?) {
        self.image = image ?? ""
        self.name = name ?? ""
    }
}
