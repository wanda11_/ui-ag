//
//  MasterRouting.swift
//  Test
//
//  Created by Hera on 30/11/20.
//

import SwiftUI

struct MasterRouting: View {
    
    // disini buat enviroment object
    @ObservedObject var varGlob = globalCore()
    var body: some View {
        if varGlob.currentPage == "mainView"{
            MainView().environmentObject(varGlob)
        }else if varGlob.currentPage == "tampilanLogin"{
            LoginView().environmentObject(varGlob)
        }else{
            Text("Gagal")
        }
    }
}

struct MasterRouting_Previews: PreviewProvider {
    static var previews: some View {
        MasterRouting()
    }
}
