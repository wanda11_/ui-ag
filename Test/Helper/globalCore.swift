//
//  globalCore.swift
//  Test
//
//  Created by Hera on 30/11/20.
//

import Foundation
import Combine
import SwiftUI

class globalCore: ObservableObject {
    var didChange = PassthroughSubject<globalCore, Never>()
    
    @Published var currentPage : String = "mainView" {didSet {self.didChange.send(self)}}
}
