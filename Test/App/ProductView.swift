//
//  ProductView.swift
//  Test
//
//  Created by Hera on 26/11/20.
//

import SwiftUI

struct ProductView: View {
    
    // MARK: - PROPERTIES
    
    // MARK: - BODY
    
    var body: some View {
        Text("Product")
    }
}

// MARK: - PREVIEW

struct ProductView_Previews: PreviewProvider {
    static var previews: some View {
        ProductView()
    }
}
