//
//  ScannerView.swift
//  Test
//
//  Created by Hera on 26/11/20.
//

import SwiftUI

struct ScannerView: View {
    
    // MARK: - PROPERTIES
    
    // MARK: - BODY
    
    var body: some View {
        NavigationView {
            GeometryReader { geo in
                ZStack {
                    Image("bg_1")
                    
                    VStack(alignment: .center, spacing: 40) {
                        Spacer()
                        Text("Verification")
                            .font(.title2)
                            .bold()
                        
                        Image(systemName: "qrcode")
                            .resizable()
                            .scaledToFit()
                            .frame(width: 100, height: 100)
                        
                        Text("Scan the QR Code on hologram to get authenticity")
                            .padding(.leading, 120)
                            .padding(.trailing, 120)
                        
                        Button(action: {
                            //
                        }, label: {
                            NavigationLink(destination: ScanView()) {
                                Text("SCAN QR CODE")
                                    .bold()
                            }
                            .navigationBarHidden(true)
                        })
                        .frame(width: geo.size.width / 1.3)
                        .padding()
                        .background(Color.accentColor)
                        .cornerRadius(25)
                        .shadow(radius: 5)
                        
                        Spacer()
                    } //: VSTACK
                } //: ZSTACK
                .frame(width: geo.size.width, height: geo.size.height)
                .foregroundColor(.white)
            }
        }
        .navigationBarHidden(true)
    }
}

// MARK: - PREVIEW

struct ScannerView_Previews: PreviewProvider {
    static var previews: some View {
        ScannerView()
    }
}
