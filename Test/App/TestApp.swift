//
//  TestApp.swift
//  Test
//
//  Created by Hera on 26/11/20.
//

import SwiftUI

@main
struct TestApp: App {
    
    var body: some Scene {
        WindowGroup {
            MasterRouting()
        }
    }
}
