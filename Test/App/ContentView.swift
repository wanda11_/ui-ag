//
//  ContentView.swift
//  Test
//
//  Created by Hera on 26/11/20.
//

import SwiftUI

struct ContentView: View {
    
    // MARK: - PROPERTIES
    
    // MARK: - BODY
    
    var body: some View {
        NavigationView {
            GeometryReader { geo in
                ScrollView(.vertical, showsIndicators: false) {
                    VStack(alignment: .center, spacing: 15) {
                        Group {
                            CoverImageView()
                                .frame(height: 275)
                                .padding(.top, 5)
                            
//                            EditCompleteProfileView()
//                                .frame(height: 176)
                            
                            PointView()
                                .frame(height: 58)
                            
                            IsiSaldoView()
                                .frame(height: 165)
                        }
                        
                        DashboardHighlightView()
                            .frame(height: 300)
                        
                        DashboardPromoView()
                            .frame(height: 270)
                        
                        DashboardFeaturedBrand()
                            .frame(height: 285)
                        
                        DashboardAGStoriesView()
                            .frame(height: 300)
                        
                        DashboardAGStoreView()
                            .frame(height: 300)
                    }
                }
            }
            .navigationBarTitle("Authentic Guards", displayMode: .inline)
            .navigationViewStyle(StackNavigationViewStyle())
        }
    }
}

// MARK: - PREVIEW

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
