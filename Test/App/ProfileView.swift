//
//  ProfileView.swift
//  Test
//
//  Created by Hera on 26/11/20.
//

import SwiftUI

struct ProfileView: View {
    
    // MARK: - PROPERTIES
    
    @EnvironmentObject var globalCore : globalCore
    
    // MARK: - BODY
    
    var body: some View {
        
        NavigationView {
            ScrollView(.vertical, showsIndicators: false) {
                VStack(alignment: .center, spacing: 10) {
                    Image(systemName: "person.crop.circle.badge.checkmark")
                        .resizable()
                        .scaledToFit()
                        .frame(width: 100, height: 100)
                        .foregroundColor(.accentColor)
                    
                    Text("Login First to Enjoy More Features")
                        .font(.system(size: 18))
                        .bold()
                    
                    Spacer()
                }
                
                
                VStack(alignment: .leading, spacing: 15) {
                    
                    Spacer()
                    
                    Group {
                        // MARK: - MY PRODUCTS
                        HStack(spacing: 20) {
                            Image(systemName: "text.badge.checkmark")
                                .resizable()
                                .scaledToFit()
                                .frame(width: 25, height: 25, alignment: .center)
                            NavigationLink(destination: NotificationView()) {
                                Text("My Products")
                            }
                            .foregroundColor(.black)
                            
                        } //: HSTACK
                        
                        Divider()
                        // MARK: - FAQ
                        HStack(spacing: 20) {
                            Image(systemName: "questionmark.circle")
                                .resizable()
                                .scaledToFit()
                                .frame(width: 25, height: 25, alignment: .center)
                            Text("FAQ")
                        } //: HSTACK
                        
                        Divider()
                    } //: GROUP
                    
                    Group {
                        // MARK: - CHANGE LANGUAGE
                        HStack(spacing: 20) {
                            Image(systemName: "globe")
                                .resizable()
                                .scaledToFit()
                                .frame(width: 25, height: 25, alignment: .center)
                            Text("Change Language")
                        } //: HSTACK
                        
                        Divider()
                        // MARK: - TERM OF SERVICES
                        HStack(spacing: 20) {
                            Image(systemName: "info.circle")
                                .resizable()
                                .scaledToFit()
                                .frame(width: 25, height: 25, alignment: .center)
                            Text("Term Of Services")
                        } //: HSTACK
                        
                        Divider()
                    } //: GROUP
                    
                    Group {
                        // MARK: - PRIVACY POLICY
                        HStack(spacing: 20) {
                            Image(systemName: "lock.shield")
                                .resizable()
                                .scaledToFit()
                                .frame(width: 25, height: 25, alignment: .center)
                            Text("Privacy Policy")
                        } //: HSTACK
                        
                        Divider()
                        // MARK: - RATE APP
                        HStack(spacing: 20) {
                            Image(systemName: "star")
                                .resizable()
                                .scaledToFit()
                                .frame(width: 25, height: 25, alignment: .center)
                            Text("Rate Authenctic Guard App")
                        } //: HSTACK
                        
                        Divider()
                    }
                    
                    GeometryReader { geo in
                        Button(action: {
                            //
                        }, label: {
                            NavigationLink(destination: LoginView()) {
                                Text("LOG IN")
                                    .bold()
                                    .font(.title3)
                                    .foregroundColor(.white)
                            }
                        })
                        .frame(width: geo.size.width / 1.1)
                        .padding()
                        .background(Color.accentColor)
                        .cornerRadius(25)
                        .shadow(radius: 5)
                        .buttonStyle(PlainButtonStyle())
                    }
                }
            }
            .padding(.horizontal)
            .navigationBarHidden(true)
        }
    }
}

// MARK: - PREVIEW

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView()
    }
}
