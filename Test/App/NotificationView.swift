//
//  NotificationView.swift
//  Test
//
//  Created by Hera on 26/11/20.
//

import SwiftUI

struct NotificationView: View {
    
    // MARK: - PROPERTIES
    
    // MARK: - BODY
    
    var body: some View {
        NotifView()
//        VStack(spacing: 15) {
//            Image("paper_plane")
//                .resizable()
//                .scaledToFit()
//                .frame(width: 100, height: 100, alignment: .center)
//                .foregroundColor(.gray)
//
//            Text("Anda belum mendapatkan pesan !")
//                .font(.system(size: 15))
//            Text("Pesan akan ditampilkan di sini")
//                .foregroundColor(.gray)
//                .font(.footnote)
//        } //: VSTACK
    }
}

// MARK: - PREVIEW

struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView()
    }
}
