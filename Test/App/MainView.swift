//
//  MainView.swift
//  Test
//
//  Created by Hera on 26/11/20.
//

import SwiftUI

struct MainView: View {
    
    // MARK: - PROPERTIES
    
    // MARK: - BODY
    @EnvironmentObject var globalCore : globalCore
    
    var body: some View {
        TabView {
            ContentView()
                .tabItem {
                    Image(systemName: "house")
                    Text("Dashboard")
                }

            ProductView()
                .tabItem {
                    Image(systemName: "bag.badge.plus")
                    Text("Product")
                }

            ScannerView()
                .tabItem {
                    Image(systemName: "qrcode")
                    Text("Scanner")
                }

            NotificationView()
                .tabItem {
                    Image(systemName: "bell")
                    Text("Notification")
                }

            ProfileView()
                .tabItem {
                    Image(systemName: "person.crop.circle")
                    Text("Profile")
                }
                .environmentObject(globalCore)
        } //: TAB
    }
}

// MARK: - PREVIEW

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
