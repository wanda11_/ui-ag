//
//  Slider.swift
//  Test
//
//  Created by Hera on 07/12/20.
//

import SwiftUI
import Foundation
import Combine

class SliderFunction: ObservableObject {
    @Published var data = [SliderModel]()
    
    {
        didSet {
            self.didChange.send(self)
            
        } // Mengeksekusi apabila terdapat perubahan data pada kelas ini
    }
    
    @Published var loading: Bool = false
    // :(untuk mendeklarasi tipe data). Jika data var ditulis = diawal tandanya default
    @Published var errorStatus: String = ""
    
    let didChange = PassthroughSubject<SliderFunction, Never>()
    
    init() {
        loadSliderAPI() // untuk mengeksekusi paling awal ketika kelas ini dipanggil
    }
    
    func loadSliderAPI() {
        loading = true
        errorStatus = ""
        let urlAPI = URL(string: "https://admin.authenticguards.com/api/slider_?&appid=003&loclang=a&loclong=a") // url API
        var requestAPI = URLRequest(url: urlAPI!)
        requestAPI.httpMethod = "GET" // metode yg digunakan
        URLSession.shared.dataTask(with: requestAPI) { [weak self] (data, response, error) -> Void in
            guard(error == nil) else { // nil (kosong / tidak ada data)
                DispatchQueue.main.async {
                    self?.loading = false
                    self?.errorStatus = "error \(String(describing: error))"
                }
                return
            } // error ketike mengirim data ke server dari handphone
            
            guard let data = data else {
                DispatchQueue.main.async {
                    self?.loading = false
                    self?.errorStatus = "Kesalahan saat mengambil data dari server"
                }
                return
            } // tidak mendapat respons data dari server
            
            do {
                if let resultJSON = try JSONSerialization.jsonObject(with: data, options : []) as? NSDictionary{ // cara mengambil data JSON
                     DispatchQueue.main.async { // agar dapat merubah data di saat aplikasi sedang berjalan
                        if (resultJSON["status"] as? String)! == "success" { // mengecek respon status dari server berhasil atau tidak
                            let resultAPI = resultJSON["result"] as! NSDictionary // cara memanggil data dari JSONSerialization yg datanya hanya terdapat satu
                            let dataAPI = resultAPI.value(forKey: "data") as! [NSDictionary] // cara memanggil data dari NSDictionary. [] = menandakan data lebih dari 1
                            var listDataAPI = [SliderModel]()
                            if dataAPI.count > 0 {
                                for hasil in dataAPI {
                                    let dataResult = SliderModel(
                                        image: hasil.value(forKey: "image") as? String,
                                        name: hasil.value(forKey: "name") as? String
                                    )
                                    listDataAPI.append(dataResult)
                                }
                                self?.data = listDataAPI
                                self?.errorStatus = ""
                                self?.loading = false
                            } else {
                                self?.errorStatus = "Tidak ada data"
                                self?.loading = false
                            }
                        } else {
                            self?.errorStatus = ""
                            self?.loading = false
                        }
                    }
                } else {
                    self?.errorStatus = "Koneksi bermasalah"
                    self?.loading = false
                }
            } catch _ as NSError {
                DispatchQueue.main.async {
                    self?.errorStatus = "Terjadi kesalahan saat mengambil data"
                    self?.loading = false
                }
            }
        }
        .resume()
    }
}
